.PHONY: local pkg build start build.api build.frontend cleanup pull

update: 
	docker pull node:12.18-alpine
	docker pull mariadb:latest
	docker pull php:8-fpm-alpine
	docker pull stripe/stripe-cli:latest
	docker pull registry.gitlab.com/crunchyintheory/select/master:latest
	touch .lockimages

pull:
	[ ! -f .lockimages ] || docker pull registry.gitlab.com/crunchyintheory/select/master:latest
	[ -f .lockimages ] || make update

build.frontend:
	[ ! -d build_frontend ] || rm -rf build_frontend
	docker run --rm -v $(shell pwd)/frontend:/app \
		-w /app \
		node:12.18-alpine \
		/bin/sh -c \
			'yarn install && \
			./node_modules/@angular/cli/bin/ng build --prod --resourcesOutputPath=./ng \
			|| (npm rebuild node-sass && ./node_modules/@angular/cli/bin/ng build --prod --resourcesOutputPath=./ng)'
	
	rsync -az --exclude index.html frontend/dist/ build_frontend
	rsync -a frontend/dist/index.html index.blade.php

build.api:
	[ -d api/vendor ] || docker run --rm -v $(shell pwd)/api:/app \
		-w /app \
		composer:2 \
		/bin/sh -c 'composer install --ignore-platform-reqs --ansi --no-interaction && composer dump-autoload'
	
	[ ! -d build_api ] || rm -rf build_api
	rsync -az \
      --exclude coverage \
      --exclude '.env*' \
      --exclude 'storage/views/*php' \
      --exclude 'storage/logs/*' \
	  --exclude 'public/app/*' \
	  --exclude 'storage/*' \
      --exclude build \
      --exclude '.git*' \
      --exclude tests \
      --exclude '*.key' \
      --exclude phpunit.xml \
      api/ \
      build_api/
	
	[ ! -f index.blade.php ] || mv index.blade.php build_api/resources/views/index.blade.php

pkg:
	rsync -avz --delete build_frontend/ build_api/public/app

build: build.frontend build.api

local: pull build
	[ -d build_frontend ] || mkdir build_frontend
	[ -d build_api ] || mkdir build_api
	[ -d build_index ] || mkdir build_index
	cp build_api/resources/views/index.blade.php build_index
	docker-compose -f docker-compose.local.yml up --build

start: pull
	docker pull registry.gitlab.com/crunchyintheory/select/master
	docker-compose up

cleanup:
	rm -rf build_frontend build_api build_index