<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

// Authentication Routes...
Route::post('/login', 'Auth\LoginController@login');
Route::post('/register', 'Auth\RegisterController@register');

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// Route::post('api/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// Route::post('api/password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/stripeKey', function() {
    return response()->json(config('services.stripe.pkey'));
});
  
Route::get('/videos/featured', 'VideoController@featured');

Route::apiResource('/packages', 'PackageController');
Route::apiResource('/videos', 'VideoController');
Route::apiResource('/tags', 'TagController');
Route::apiResource('/subscriptions', 'SubscriptionController');

Route::get('/videos/{video}/canWatch', 'VideoController@canWatch');

Route::get('/settings', 'SettingsController@index');

Route::get('/sanctum/csrf-cookie', [\Laravel\Sanctum\Http\Controllers\CsrfCookieController::class, 'show']);

Route::group(['middleware' => ['auth:sanctum']], function() {

  Route::apiResource('/promos', 'PromoController');
  Route::apiResource('/gift-cards', 'GiftCardController');
  Route::apiResource('/users', 'UserController');
  Route::get('/checkout', 'CheckoutController@createPurchaseSession')->middleware('verified');
  Route::get('/videos/{video}/watch', 'VideoController@getWatchUrl');

  Route::put('/user/cart', 'UserController@addToCart')->middleware('verified');
  Route::get('/user/cart', 'UserController@getCart');
  Route::delete('/user/cart/{video}', 'UserController@removeFromCart');
  Route::get('/user/upload', 'UserController@upload');
  Route::get('/user', 'UserController@getCurrent');
  Route::get('/user/videos', 'UserController@videos');
  Route::post('/user/redeem', 'UserController@redeem')->middleware('verified');
  Route::get('/user/libnew', 'UserController@newInLibrary');

  Route::get('/dashboard', function(Request $request) {
    if (!$request->user()->can('admin')) {
      return array();
    }
    return array(
      'videos' => App\Video::count(),
      'packages' => App\Package::count(),
      'users' => App\User::count()
    );
  });

  Route::delete('/packages/{package}/force', 'PackageController@forceDelete');
  Route::post('/packages/{package}/restore', 'PackageController@restore');
  Route::delete('/videos/{video}/force', 'VideoController@forceDelete');
  Route::post('/videos/{video}/restore', 'VideoController@restore');
  Route::post('/videos/featured', 'VideoController@setFeatured');

  Route::put('/settings', 'SettingsController@update');

  Route::post('/cart/promo', 'CheckoutController@applyPromo');
  Route::delete('/cart/promo', 'CheckoutController@removePromo');

  Route::apiResource('checkout.subscriptions', 'SubscriptionController');
  Route::post('/checkout/subscriptions/{subscription}/session', 'SubscriptionController@checkout')->middleware('verified');
  Route::get('/checkout/stripeSubscriptions', 'CheckoutController@subscriptions');

  Route::post('/checkout/portal', 'UserController@createPortalSession')->middleware('verified');
});