<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test', function() { \Illuminate\Support\Facades\Mail::to(\App\User::first())->send(new \App\Mail\Receipt(\App\CheckoutSession::first())); });
Route::post('/api/stripe/webhook', '\App\Http\Controllers\WebhookController@handleWebhook');

$serveFrontend = function() {

  return View::make('index');

};

Route::get('/email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');

Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/update', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('/app/payment/complete', $serveFrontend)->name('payment.complete');

Route::get('/app/login', $serveFrontend)->name('login');

Route::get('/app/videos/{video}/watch', $serveFrontend)->name('videos.watch');

Route::get('/sitemap.xml', function() {
  $data = [];
  $data['lastvideo'] = \App\Video::orderBy('created_at', 'desc')->first()->created_at;
  $data['lastpackage'] = \App\Package::orderBy('created_at', 'desc')->first()->created_at;
  $data['videos'] = \App\Video::all();
  return response()->view('sitemap', $data)->header('Content-Type', 'text/xml');
});

Route::get('/{any}', $serveFrontend)->where('any', '.*')->name('home');
