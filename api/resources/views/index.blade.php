<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <title>Select</title>
  <base href="/app/">
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <link rel="icon" type="image/x-icon" href="favicon.ico"/>
  <meta name="csrf-token" content="{!! csrf_token() !!}"/>
</head>
<body>
  <app-root></app-root>
<script src="runtime.js" type="module"></script><script src="polyfills.js" type="module"></script><script src="styles.js" type="module"></script><script src="vendor.js" type="module"></script><script src="main.js" type="module"></script></body>
  <script data-cfasync="false" defer type="text/javascript" src="https://embed.cloudflarestream.com/embed/r4xu.fla9.latest.js"></script>
</html>
