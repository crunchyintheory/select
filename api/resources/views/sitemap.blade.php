<<?='?'?>xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ url('/') }}</loc>
        <priority>1.0</priority>
        <lastmod>{{ $lastvideo->max($lastpackage)->format('Y-m-d\TH:i:sP') }}</lastmod>
    </url>
    <url>
        <loc>{{ url('app/home') }}</loc>
        <priority>0.9</priority>
        <lastmod>{{ $lastvideo->max($lastpackage)->format('Y-m-d\TH:i:sP') }}</lastmod>
    </url>
    <url>
        <loc>{{ url('app/login') }}</loc>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>{{ url('app/register') }}</loc>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>{{ url('app/videos') }}</loc>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
        <lastmod>{{ $lastvideo->format('Y-m-d\TH:i:sP') }}</lastmod>
    </url>
    <url>
        <loc>{{ url('app/packages') }}</loc>
        <priority>0.9</priority>
        <changefreq>daily</changefreq>
        <lastmod>{{ $lastpackage->format('Y-m-d\TH:i:sP') }}</lastmod>
    </url>
    @foreach ($videos as $video)
    <url>
        <loc>{{ url("app/videos/{$video->id}") }}</loc>
        <lastmod>{{ $video->updated_at->format('Y-m-d\TH:i:sP') }}</lastmod>
        <changefreq>yearly</changefreq>
        <priority>0.7</priority>
    </url>
    @endforeach
</urlset>