@component('mail::message')
<h1>Hello!</h1>

Thank you for your purchase on {{ config('app.name') }}!

<table style="table-layout: fixed; width: 100%; padding-top: 20px;">
@foreach($videos as $video)
    <tr>
        <td class="db-sm" style="width: 152px; padding-top: 20px">
            <img src="{{ $video->thumbnail->url }}" width="150" style="border-radius: 8px" />
        </td>
        <td class="db-sm" style="vertical-align: top; padding-left: 20px; padding-top: 20px">
            <h1><a href="{{ route('videos.watch', ['video' => $video]) }}" target="_blank">{{ $video->name }}</a></h1>
        </td>
        <td style="width: 75px; padding-top: 20px">
            <h2 style="text-align: right">${{ sprintf('%.2F', $video->price / 100) }}</h2>
        </td>
    </tr>
@endforeach
</table>
@if($deduction)
<h1 style="text-align: right; margin-top: 20px; margin-bottom: 0">Subtotal:&nbsp;&nbsp;<span style="color: #718096">${{ sprintf('%.2F', $subtotal / 100) }}</span></h1>
<h1 style="font-size: 0.9rem; text-align: right; margin-top: 10px; margin-bottom: 0">Coupons and Balance:&nbsp;&nbsp;<span style="color: #718096">-${{ sprintf('%.2F', $deduction / 100) }}</span></h1>
@endif
<h1 style="font-size: 1.2rem; text-align: right; margin: 10px 0 20px">Total:&nbsp;&nbsp;<span style="color: #718096">${{ sprintf('%.2F', ($subtotal - $deduction) / 100) }}</span></h1>


As a reminder, these videos will expire after 72 hours from the time of purchase, and you will have 24 hours to finish them after you start watching.

<br/>
Regards,
{{ config('app.name') }}

@endcomponent