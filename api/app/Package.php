<?php

namespace App;

use App\Video;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $name
 * @property string $description
 * @property string $bgcolor
 * @property integer $price
 * @property-read Media $media
 * @property-read \Illuminate\Support\Collection $tags
 * @property-read \Illuminate\Support\Collection $videos
 * @property-read integer $price
 */
class Package extends Model
{
    use UsesUuid, SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'bgcolor',
        'price'
    ];

    protected $casts = [
        'price' => 'integer'
    ];

    protected static function booted() {
        static::forceDeleted(function ($package) {
            $package->tags()->detach();
            $package->videos()->detach();
        });
    }

    public function media() {
        return $this->morphOne(Media::class, 'mediable');
    }

    public function tags() {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function videos() {
        return $this->belongsToMany(Video::class);
    }

    public function getPriceAttribute() {
        return $this->videos->sum->price;
    }
}
