<?php

namespace App\Mail;

use App\CheckoutSession as Session;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Receipt extends Mailable
{
    use Queueable, SerializesModels;

    private $session;
    private $videos;
    private $subtotal;
    private $deduction;

    public function __construct(Session $session)
    {
        $this->session = $session;
        $this->videos = $session->videos()->with('thumbnail')->get();
        
        $lines = \Stripe\Checkout\Session::allLineItems($this->session->stripe_id);

        $this->subtotal = $this->videos->sum->price;
        $this->deduction = ($this->subtotal - collect($lines->data)->pluck('price')->sum->unit_amount);
    }

    public function build()
    {
        return $this->subject('Payment Receipt')->markdown('emails.receipt', ['videos' => $this->videos, 'session' => $this->session, 'subtotal' => $this->subtotal, 'deduction' => $this->deduction]);
    }
}
