<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property \Carbon\Carbon $expires
 * @property boolean $has_started
 */
class UserVideo extends Pivot
{
    public $increments = true;

    public $fillable = [
        'expires',
        'has_started'
    ];

    public $casts = [
        'expires' => 'datetime',
        'has_started' => 'boolean'
    ];
}