<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public $table = 'subs';
    public $fillable = [
        'name',
        'price',
        'credit',
        'price_id',
        'description'
    ];
}
