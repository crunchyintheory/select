<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $recordable_type
 * @property string $recordable_id
 * @property-read mixed $recordable
 * @property string $user_id
 * @property-read User $user
 * @property string $log
 */
class Record extends Model
{
    protected $fillable = [
        'recordable_type',
        'recordable_id',
        'user_id',
        'log'
    ];

    protected $hidden = [
        'id'
    ];

    public function recordable() {
        return $this->morphTo();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
