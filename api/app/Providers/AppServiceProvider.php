<?php

namespace App\Providers;

use App\Video;
use App\Observers\VideoObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::connection()->setQueryGrammar(new \App\Database\Query\Grammars\MySqlGrammar);

        \Stripe\Stripe::setApiKey(config('services.stripe.skey'));

        Video::observe(VideoObserver::class);

        Paginator::useBootstrap();
    }
}
