<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $stripe_id
 * @property string $user_id
 * @property User $user
 * @property boolean $fulfilled
 * @property integer $deduct
 * @property boolean $failed
 */
class CheckoutSession extends Model
{
    public $fillable = [
        'stripe_id',
        'user_id',
        'subscription_id',
        'fulfilled',
        'deduct',
        'failed'
    ];

    public $casts = [
        'deduct' => 'integer',
        'failed' => 'boolean',
        'fulfilled' => 'boolean'
    ];

    protected static function booted() {
        static::addGlobalScope('unfulfilled', function (Builder $builder) {
            $builder->where('fulfilled', false);
        });
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function videos() {
        return $this->belongsToMany('App\Video')->withPivot(['expires']);
    }

    public function subscription() {
        return $this->belongsTo(Subscription::class);
    }
}
