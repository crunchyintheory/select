<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property \Carbon\Carbon $expires
 */
class CheckoutSessionVideo extends Pivot
{
    public $increments = true;

    public $fillable = [
        'expires'
    ];

    public $casts = [
        'expires' => 'datetime'
    ];
}