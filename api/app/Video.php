<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Bouncer;
use Carbon\Carbon;

/**
 * @property string $name
 * @property string $description
 * @property integer $duration
 * @property integer $duration_tag_id
 * @property string $status
 * @property string $videouid
 * @property string $uploader_id
 * @property integer $price
 * @property string $product_id
 * @property string $cfstate
 * 
 * @property-read \Illuminate\Support\Collection $users
 * @property-read User $uploader
 * @property-read \Illuminate\Support\Collection $tags
 * @property-read Media $thumbnail
 * @property-read \Illuminate\Support\Collection $packages
 * @property-read Tag $duration_tag
 * @property-read \Stripe\Product $product
 * @property-read \Illuminate\Support\Collection $carts
 * @property-read \Illuminate\Support\Collection $checkout_sessions
 * @property-read \Illuminate\Support\Collection $records
 */
class Video extends Model
{
    use UsesUuid, SoftDeletes;

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);

        Bouncer::ownedVia(Video::class, function ($video, $user) {
            return $video->price === 0 || $user->videos->where('pivot.expires', '>', Carbon::now())->pluck('id')->contains($video->id);
        });
    }

    protected static function booted() {
        static::addGlobalScope('published', function (Builder $builder) {
            if(!Auth::user() || !Auth::user()->can('viewDraft', Video::class)) {
                $builder->where('status', 'published');
                $builder->where('product_id', '!=', '');
                $builder->where('cfstate', 'ready');
            }
        });
    }

    protected $fillable = [
        'name',
        'description',
        'duration', //Seconds
        'duration_tag_id',
        'status',
        'videouid',
        'uploader_id',
        'price',
        'product_id',
        'cfstate'
    ];

    protected $hidden = [
        'videouid', 'product_id'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'price' => 'integer',
        'duration' => 'integer'
    ];

    public function users() {
        return $this->belongsToMany(User::class)->using(UserVideo::class)->withPivot(['expires']);
    }

    public function uploader() {
        return $this->belongsTo(User::class, 'id', 'uploader_id');
    }

    public function tags() {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function thumbnail() { 
        return $this->morphOne(Media::class, 'mediable');
    }

    public function packages() {
        return $this->belongsToMany(Package::class);
    }

    public function duration_tag() {
        return $this->belongsTo(Tag::class);
    }

    public function getProductAttribute() {
        return \Stripe\Product::retrieve($this->product_id);
    }

    public function carts() {
        return $this->belongsToMany(User::class, 'carts')->withPivot(['added_price']);
    }

    public function checkout_sessions() {
        return $this->belongsToMany(CheckoutSession::class);
    }

    public function records() {
        return $this->morphMany(Record::class, 'recordable')->orderBy('created_at', 'desc');
    }
}
