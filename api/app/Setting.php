<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $value
 * @property boolean $hidden
 */
class Setting extends Model
{
    public $fillable = [
        'name',
        'value',
        'hidden'
    ];

    public static function get(string $name, bool $allowHidden = false) {
        $s = Setting::where('name', $name);
        if (!$allowHidden) {
            $s->where('hidden', false);
        }

        return $s->first() ? $s->first()->value : null;
    }
}
