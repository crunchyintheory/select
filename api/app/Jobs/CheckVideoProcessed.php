<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Video;
use App\Tag;
use App\Media;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use App\Classes\Cloudflare;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class CheckVideoProcessed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $videos = Video::withoutGlobalScope('published')->whereNotIn('cfstate', ['error', 'ready'])->get();
        foreach($videos as $video) {
            try {
                $res = Http::withHeaders(array(
                    'Authorization' => sprintf('Bearer %s', config('services.cloudflare.key')),
                    'Content-Type' => 'application/json'
                ))
                ->post(
                    sprintf('https://api.cloudflare.com/client/v4/accounts/%s/media/%s', config('services.cloudflare.account'), $video->videouid),
                    array(
                        'uid' => $video->videouid,
                        'requireSignedURLs' => true
                    )
                );
    
                if(!$res || $res->failed()) {
                    Log::error(sprintf('A cloudflare error occured for video ID %s (%s)', $video->id, @$res->status()));
                    continue;
                }

                $data = $res->object()->result;

                if(!$video->thumbnail) {
                    $signed = Cloudflare::sign($video, User::first(), Carbon::now()->add(10, 'minutes'));
                    Storage::disk('thumbnails')->put($fname = sprintf('%s_sd.jpg', $video->id), Http::get(sprintf('https://videodelivery.net/%s/thumbnails/thumbnail.jpg', $signed)));
                    Storage::disk('thumbnails')->put(sprintf('%s_hd.jpg', $video->id), Http::get(sprintf('https://videodelivery.net/%s/thumbnails/thumbnail.jpg?width=1920', $signed)));
                    
                    $video->thumbnail()->save(new Media(array(
                        'filename' => sprintf('thumbnails/%s', $fname),
                        'uploader_id' => $video->uploader_id
                    )));
                }

        
                $video->fill([
                    'cfstate' => $data->status->state,
                    'duration' => max($data->duration, 0)
                ]);

                $video->duration_tag()->associate(Tag::where('time_threshold', '<=', max(round($data->duration / 900 /* 15 * 60 */), 1) * 900)->orderByDesc('time_threshold')->first());
                $video->save();
            } catch(\Throwable $e) {
                Log::error($e->__toString());
                continue;
            }
        }
    }
}
