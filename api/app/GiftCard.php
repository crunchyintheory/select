<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $redeemed_by_id
 * @property-read User $redeemed_by
 * @property integer $value
 */
class GiftCard extends Model
{
    public $fillable = [
        'name',
        'redeemed_by_id',
        'value'
    ];

    public $hidden = [
        'redemption'
    ];

    public $casts = [
        'value' => 'integer'
    ];

    public static function booted() {
        static::created(function($giftcard) {
            $redemption = str_split($rnd = bin2hex(random_bytes(8)));
            $redemption[0] = $rnd[0] = 'f'; // Version the gift card code, in case the algorithm needs to be changed.
            $random = hexdec(substr(hash('sha256', $rnd), 0, 4));
            $i = 1;
            foreach($redemption as &$letter) {
                ($i & $random) && $letter = strtoupper($letter);
                $i = $i << 1;
            }
            $redemption = str_split(implode('', $redemption), 4);
            $giftcard->redemption = implode('-', $redemption);
            $giftcard->save();
        });
    }

    public function redeemed_by() {
        return $this->belongsTo(User::class);
    }
}
