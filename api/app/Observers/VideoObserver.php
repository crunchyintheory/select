<?php

namespace App\Observers;

use App\Video;
use App\Cart;

class VideoObserver
{
    /**
     * Handle the video "created" event.
     *
     * @param  \App\Video  $video
     * @return void
     */
    public function created(Video $video)
    {
        $product = \Stripe\Product::create([
            'name' => $video->name,
            'description' => '72 Hours to start watching, 24 hours to finish watching after you start.',
            'metadata' => [
                'video_id' => $video->id
            ]
        ]);

        $video->update(['product_id' => $product->id]);
    }

    /**
     * Handle the video "updated" event.
     *
     * @param  \App\Video  $video
     * @return void
     */
    public function updated(Video $video)
    {
        \Stripe\Product::update($video->product_id, [
            'name' => $video->name
        ]);
        if ($video->status !== 'published') {
            $video->carts->each->delete();
        } else {
            Cart::where('added_price', '!=', $video->price)->get()->each->delete();
        }
        $video->checkout_sessions->each->delete();
    }

    /**
     * Handle the video "deleted" event.
     *
     * @param  \App\Video  $video
     * @return void
     */
    public function deleted(Video $video)
    {
        $video->carts()->detach();
        $video->checkout_sessions()->detach();
        // Cart::where('video_id', $video->id)->get()->each->delete();
        // CheckoutSessionVideo::where('video_id', $video->id)->get()->each->delete();
    }

    /**
     * Handle the video "restored" event.
     *
     * @param  \App\Video  $video
     * @return void
     */
    public function restored(Video $video)
    {
        //
    }

    /**
     * Handle the video "force deleted" event.
     *
     * @param  \App\Video  $video
     * @return void
     */
    public function forceDeleted(Video $video)
    {
        $video->tags()->detach();
        $video->packages()->detach();
        $video->users()->detach();
    }
}
