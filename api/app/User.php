<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Auth\Passwords\CanResetPassword;

/**
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property integer $balance
 * @property integer $promo_id
 * @property-read Promo $promo
 * @property boolean $has_seen_cart_warning
 * 
 * @property-read \Illuminate\Support\Collection $videos
 * @property-read \Illuminate\Support\Collection $cart
 * @property-read Promo $promo
 * @property-read \Illuminate\Support\Collection $sessions
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, Billable, HasRolesAndAbilities, UsesUuid, SoftDeletes, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'balance', 'promo_id', 'has_seen_cart_warning', 'subscription_end'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at', 'stripe_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'balance' => 'integer',
        'has_seen_cart_warning' => 'boolean',
        'subscribed' => 'boolean',
        'subscription_end' => 'datetime'
    ];

    protected $appends = [
        'subscribed'
    ];

    public function videos() {
        return $this->belongsToMany(Video::class)->using(UserVideo::class)->withPivot(['expires', 'has_started']);
    }

    public function cart() {
        return $this->belongsToMany(Video::class, 'carts')->withPivot(['added_price']);
    }

    public function promo() {
        return $this->belongsTo(Promo::class);
    }

    public function sessions() {
        return $this->hasMany(CheckoutSession::class);
    }

    public function getSubscribedAttribute() {
        return $this->subscription_end && $this->subscription_end->timestamp > time();
    }
}
