<?php

namespace App\Http\Controllers;

use App\Video;
use App\Setting;
use App\Tag;
use App\Record;
use App\Classes\Cloudflare;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VideoController extends Controller
{
    public function __construct() {
        $this->authorizeResource(Video::class);
    }

    protected function storeValidator($id = null) {
        return [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:1000',
            'status' => ['nullable', Rule::in(['published', 'draft'])],
            'videouid' => [Rule::unique('videos', 'videouid')->ignore($id)],
            'tags' => 'array',
            'price' => 'numeric|regex:/^\d+(?:\.\d{1,2})?$/'
        ];
    }

    protected function updateValidator() {
        return [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:1000',
            'status' => ['nullable', Rule::in(['published', 'draft'])],
            'tags' => 'array',
            'price' => 'numeric|regex:/^\d+(?:\.\d{1,2})?$/'
        ];
    }

    protected function resourceAbilityMap() {
        return [
            // 'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = Video::with('thumbnail', 'tags', 'duration_tag');
        if($request->user() && $request->user()->can('admin')) {
            $q->with('records');
        }
        $q->skip($request->get('skip', 0))->take($request->get('take', 10));
        if ($request->input('search')) {
            $q->where('name', 'like', sprintf('%%%s%%', $request->input('search')));
        }
        if ($request->input('deleted', 'false') !== 'false' && $request->user()->can('manage', Video::class)) {
            $q->withTrashed();
        }

        $tags = collect($request->input('tags') ? explode(',', $request->input('tags')) : []);
        $duration_tags = Tag::where('time_threshold', '!=', null)->whereIn('id', $tags)->get()->pluck('id');
        $tags = $tags->diff($duration_tags);

        if ($tags->count()) {
            $q->select(DB::raw('videos.*, COUNT(taggables.tag_id) as tagmatch'));
            $q->join('taggables', 'videos.id' , '=', 'taggables.taggable_id');
            $q->whereIn('taggables.tag_id', $tags);
            $q->groupBy('videos.id');
        }
        if ($duration_tags->count()) {
            $q->whereIn('videos.duration_tag_id', $duration_tags);
        }
        $q->orderBy('created_at', 'desc');
        $videos = $q->get();
        if($request->input('tags')) {
            $videos = $videos->where('tagmatch', '>=', count($tags))->values();
        }
        return response()->json($videos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate($this->storeValidator());
        
        $validated['duration'] = 0;

        $validated['tags'] = collect($validated['tags'])->pluck('id');

        $validated['price'] = floor(floatval($validated['price'] * 100));

        $validated['uploader_id'] = Auth::user()->id;

        $video = new Video($validated);

        $video->save();

        $video->tags()->sync($validated['tags']);

        return response()->json($video, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video, Request $request)
    {
        if($request->user() && $request->user()->can('admin')) {
            $video->loadMissing(['records', 'records.user']);
        }
        return response()->json($video->loadMissing(['thumbnail', 'tags', 'packages', 'duration_tag']), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $validated = $request->validate($this->updateValidator());

        $validated['tags'] = collect($validated['tags'])->pluck('id');

        $validated['price'] = floor(floatval($validated['price'] * 100));

        $video->update($validated);

        $video->tags()->sync($validated['tags']);

        return response()->json($video);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        $video->delete();
    }

    public function forceDelete($video) {
        $vid = Video::withTrashed()->find($video);

        $this->authorize('manage', $vid);

        $vid->forceDelete();
    }

    public function restore($video) {
        $vid = Video::withTrashed()->find($video);

        $this->authorize('manage', $vid);

        $vid->restore();
    }

    public function canWatch(Video $video, Request $request)
    {
        if($request->user() && $request->user()->can('watch', $video)) {
            return response(200);
        }

        return response(401);
    }

    public function getWatchUrl(Video $video, Request $request)
    {
        $this->authorize('watch', $video);

        $user = $request->user();

        $viduser = $user->videos->keyBy('id')->get($video->id);

        if (!$user->isA('admin') && (!$viduser || $viduser->pivot->expires <= Carbon::now())) {
            return response()->error('This video has expired.', 402);
        }

        if($viduser && !$viduser->pivot->has_started) {
            Record::firstOrCreate(['log' => 'started_watching', 'user_id' => $user->id, 'recordable_id' => $video->id, 'recordable_type' => 'App\Video']);
            $user->videos()->updateExistingPivot($video->id, ['expires' => Carbon::now()->add(1, 'day'), 'has_started' => true]);
        }

        $userexp = !$user->isA('admin') ? $viduser->pivot->expires : Carbon::now()->add(1, 'day');
        $exp = max($userexp, Carbon::now()->add($video->duration + 60, 'seconds'));
        
        return response()->json(['token' => Cloudflare::sign($video, $user, $exp)], 200);
    }

    public function featured()
    {
        $video = Video::with('thumbnail', 'tags', 'duration_tag')->find(Setting::get('featured_video', false));

        if(!$video) {
            return response()->json(null, 204);
        }

        return response()->json($video, 200);
    }

    public function setFeatured(Request $request) {
        $this->authorize('manage', Video::class);

        $f = Setting::get('featured_video', false);
        $video = Video::find($request->input('video'));
        if (!$video) {
            return response('', 404);
        }

        if ($f === $video->id) {
            Setting::where('name', 'featured_video')->delete();
            return null;
        } else {
            Setting::updateOrCreate(
                ['name' => 'featured_video'],
                ['value' => $video->id]
            );
            return $video;
        }
    }
}
