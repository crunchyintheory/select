<?php

namespace App\Http\Controllers;

use App\CheckoutSession as Session;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\Receipt;
use App\Subscription;
use Throwable;

class WebhookController extends CashierController
{
    /**
     * @param Session $session
     */
    protected static function fail($session, $checkout) {
        \Stripe\Refund::create([
            'payment_intent' => $checkout['payment_intent']
        ]);
        $session->update(['failed' => true]);
        return response('', 409);
    }

    public function handleCheckoutSessionCompleted($payload) {
        $checkout = $payload['data']['object'];
        $user = Cashier::findBillable($checkout['customer']);

        if($user == null) {
            return response('', 417);
        }

        $session = Session::where('stripe_id', $checkout['id'])->first();

        if(!$session) {
            return;
        }

        if($session->fulfilled || $session->failed) {
            return static::fail($session, $checkout);
        }

        if ($session->deduct) {
            if ($user->balance >= $session->deduct) {
                $user->update(['balance' => $user->balance - $session->deduct]);
            } else {
                return static::fail($session, $checkout);
            }
        }

        if($session->subscription) {

            // $user->update(['subscription_end' => \Carbon\Carbon::now()->add(1, 'month')]);

        } else if(count($session->videos)) {

            $success = CheckoutController::AddVideosToAccount($user, $session->videos);

            if(!$success) {
                \Stripe\Refund::create([
                    'payment_intent' => $checkout['payment_intent']
                ]);
                return response('', 409);
            }

            $session->update(['fulfilled' => true]);

            try {
                Mail::to($user)->send(new Receipt($session));
            } catch (\Throwable $e) {
                Log::error("Could not send receipt {$session->id} to {$user->email}");
            }

            return response('', 200);

        } else {
            \Stripe\Refund::create([
                'payment_intent' => $checkout['payment_intent']
            ]);
            return response('', 422);
        }
    }

    public function handleCustomerSubscriptionUpdated($payload) {
        /**
         * @var \Stripe\Subscription $sub
         */
        $sub = $payload['data']['object'];
        $user = Cashier::findBillable($sub['customer']);
        
        switch($sub['status']) {
            case 'incomplete': break;
            case 'incomplete_expired': break;
            case 'trialing': break;
            case 'active':
                $user->update(['subscription_end' => $sub['current_period_end']]);
                break;
            case 'past_due': break;
            case 'canceled': 
                $user->update(['subscription_end' => time()]);
            case 'unpaid': break;
        }

        return response('', 200);
    }

    public function handleInvoicePaid($payload) {
        /**
         * @var \Stripe\Invoice $invoice
         * @var \Stripe\Subscription $sub
         */
        $invoice = $payload['data']['object'];
        $user = Cashier::findBillable($invoice['customer']);
        $sub = \Stripe\Subscription::retrieve($invoice['subscription']);
        $subscription = Subscription::where('price_id', $sub['items']['data'][0]['price']['id'])->first();

        $subs = collect(\Stripe\Subscription::all(['customer' => $invoice['customer'], 'status' => 'active'])->data)->where('id', '!=', $sub->id);
        if($subs->count()) {
            try {
                \Stripe\Refund::create([
                    'charge' => $invoice['charge'],
                    'reason' => 'duplicate'
                ]);
                $sub->cancel(['invoice_now' => false, 'prorate' => false]);
                return response('', 409);
            } catch(Throwable $e) {
                return response('', 409);
            }
        }

        if(!$subscription) {
            return response('', 404);
        }

        Log::info('Handling subscription payment for ' . $user->id);

        $user->update(['balance' => $user->balance + $subscription->credit, 'subscription_end' => $sub->current_period_end]);

        return response('', 200);
    }

    public function handleCustomerSubscriptionDeleted($payload) {
        /**
         * @var \Stripe\Subscription $sub
         */
        $sub = $payload['data']['object'];
        $user = Cashier::findBillable($sub['customer']);

        $subs = \Stripe\Subscription::all(['customer' => $sub['customer'], 'status' => 'active']);
        if(!$subs->count()) {
            $user->update(['subscription_end' => $sub['ended_at']]);
        }

        return response('', 200);
    }
}
