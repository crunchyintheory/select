<?php

namespace App\Http\Controllers;

use App\Promo;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    public function __construct() {
        $this->authorizeResource(Promo::class);
    }

    protected function validator() {
        return [
            'code' => 'required|string',
            'discount' => 'numeric|min:0|max:100',
            'expires' => 'date'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = Promo::query()->orderBy('expires', 'desc');
        $q->skip($request->get('skip', 0))->take($request->get('take', 10));
        if ($request->input('search')) {
            $q->where('code', 'like', sprintf('%%%s%%', $request->input('search')));
        }

        return response()->json($q->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate($this->validator());

        $validated['discount'] = floor($validated['discount'] * 100);

        $promo = new Promo($validated);

        $promo->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function show(Promo $promo)
    {
        return response()->json($promo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promo $promo)
    {
        $validated = $request->validate($this->validator());

        $validated['discount'] = floor($validated['discount'] * 100);

        $promo->update($validated);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promo $promo)
    {
        $promo->delete();
    }
}
