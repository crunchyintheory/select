<?php

namespace App\Http\Controllers;

use App\GiftCard;
use Illuminate\Http\Request;

class GiftCardController extends Controller
{
    public function __construct() {
        $this->authorizeResource(GiftCard::class, 'gift_card');
    }

    protected function validator() {
        return [
            'name' => 'required|string|max:255',
            'value' => 'numeric|min:1',
            'count' => 'integer|min:1'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = GiftCard::with('redeemed_by')->orderBy('redeemed_by_id')->orderBy('name');
        $q->skip($request->get('skip', 0))->take($request->get('take', 10));
        if ($request->input('search')) {
            $q->where('name', 'like', sprintf('%%%s%%', $request->input('search')));
        }

        return response()->json($q->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate($this->validator());

        $validated['value'] = floor($validated['value'] * 100);

        for($i = 0; $i < $validated['count']; $i++) {
            $giftcard = new GiftCard($validated);
            $giftcard->save();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GiftCard  $gift_card
     * @return \Illuminate\Http\Response
     */
    public function show(GiftCard $gift_card)
    {
        $ret = $gift_card->toArray();
        $ret['redemption'] = $gift_card->redemption;
        return response()->json($ret);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GiftCard  $gift_card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GiftCard $gift_card)
    {
        $validated = $request->validate($this->validator());

        $validated['value'] = floor($validated['value'] * 100);

        if ($gift_card->redeemed_by_id) {
            return response(410);
        }

        $gift_card->update($validated);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GiftCard  $giftcard
     * @return \Illuminate\Http\Response
     */
    public function destroy(GiftCard $gift_card)
    {
        $gift_card->delete();
    }
}
