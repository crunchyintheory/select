<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\CheckoutSession as Session;
use App\Promo;
use App\User;
use App\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Cashier;
use Carbon\Carbon;

class CheckoutController extends Controller
{
    public static function AddVideosToAccount(User $user, \Illuminate\Database\Eloquent\Collection  $videos) {
        $activeVids = $user->videos->filter(function(&$el) { return $el->pivot->expires >= Carbon::now(); });
            
        if (count($videos->intersect($activeVids))) {
            return false;
        }

        $purchased = $videos->keyBy('id');

        $renewable = $user->videos->keyBy('id')->intersectByKeys($purchased);
        $new = $purchased->except($renewable->pluck('id')->all()); // No longer keyed!

        foreach($renewable as $id => $video) {
            Record::firstOrCreate(['log' => 'repurchased', 'user_id' => $user->id, 'recordable_id' => $video->id, 'recordable_type' => 'App\Video']);
            $user->videos()->updateExistingPivot($video, array('expires' => $purchased[$id]->pivot->expires));
            $user->cart()->detach($video->id);
        }

        foreach($new as $video) {
            Record::firstOrCreate(['log' => 'purchased', 'user_id' => $user->id, 'recordable_id' => $video->id, 'recordable_type' => 'App\Video']);
            $user->videos()->attach($video->id, ['expires' => $video->pivot->expires]);
            $user->cart()->detach($video->id);
        }

        $user->update(['promo_id' => null]);

        return true;
    }

    public function createPurchaseSession(Request $request) {
        /*if (!$request->session()->has('cart')) {
            return response('', 411);
        }
        $cart = $request->session()->get('cart');*/

        $user = $request->user();

        $user->sessions->where('fulfilled', false)->each->delete();

        $videos = $user->cart->keyBy('id')->except($user->videos->where('pivot.expires', '>', Carbon::now())->pluck('id')->all());

        if (count($videos) === 0) {
            return response('', 422);
        }

        $customer = $user->createOrGetStripeCustomer();

        $discount = ($user->promo && $user->promo->expires > Carbon::now()) ? 1 - ($user->promo->discount / 10000) : 1;

        $total = intval($videos->sum->price) * $discount;
        $deduct = 0;
        $line_items = [];

        foreach($videos as $video) {
            $deduct += $thisdeduction = min(intval($price = $video->price * $discount), $user->balance - $deduct);
            /*if ($deduct === $price) {
                continue;
            }*/
            if ($thisdeduction === intval($price)) {
                continue;
            }
            $line_items[] = [
                'price_data' => [
                    'currency' => 'usd',
                    'product' => $video->product_id,
                    'unit_amount' => $price - $thisdeduction
                ],
                'quantity' => 1
            ];
        }

        if($deduct === $total) {
            $user->update(['balance' => $user->balance - $deduct]);

            $videos->transform(function(&$el) { $el->pivot = (object)array('expires' => Carbon::now()->add(3, 'days')); return $el; });

            $success = static::AddVideosToAccount($user, $videos);
            if (!$success) {
                $user->update(['balance' => $user->balance + $deduct]);
                return response('', 409);
            }

            $user->refresh();

            return response($user, 200);
        }

        $stripesession = \Stripe\Checkout\Session::create([
            'customer' => $customer->id,
            'payment_method_types' => ['card'],
            'line_items' => $line_items,
            'mode' => 'payment',
            'success_url' => route('payment.complete') . '/{CHECKOUT_SESSION_ID}',
            'cancel_url' => route('home', ['any' => 'app/checkout'])
        ]);

        $session = new Session([
            'stripe_id' => $stripesession->id,
            'user_id' => $user->id,
            'fulfilled' => false,
            'deduct' => $deduct
        ]);

        $session->deduct = $deduct ?? 0;

        $session->save();

        foreach($videos as $video) {
            $session->videos()->attach($video, [
                'expires' => Carbon::now()->add(3, 'days')
            ]);
        }

        return response()->json($stripesession->id, 201);
    }

    public function applyPromo(Request $request) {
        $promo = Promo::where('code', $request->input('code'))->get();
        
        if (!$promo->count()) {
            return response('', 404);
        }

        $promo = $promo[0];

        $request->user()->update(['promo_id' => $promo->id]);

        return response($promo, 202);
    }

    public function removePromo(Request $request) {
        $request->user()->update(['promo_id' => null]);
    }

    public function subscriptions() {
        $this->authorize('admin');

        $subscriptions = \Stripe\Price::all([
            'active' => true,
            'product' => SubscriptionController::getSubscriptionProduct()->id,
            'currency' => 'USD',
            'type' => 'recurring',
            'recurring' => ['interval' => 'month']
        ]);

        if(!isset($subscriptions->data)) {
            return response()->json([], 200);
        }

        return response()->json($subscriptions->data);
    }
}
