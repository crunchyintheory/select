<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    public function __construct() {
        $this->authorizeResource(Package::class);
    }
    
    protected function storeValidator() {
        return $this->updateValidator();
    }

    protected function updateValidator() {
        return [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:1000',
            'tags' => 'array',
            'videos' => 'array',
            'bgcolor' => 'string|regex:/^#[a-fA-F0-9]{6}$/'
        ];
    }

    protected function resourceAbilityMap() {
        return [
            'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete'
        ];
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = Package::with(['media', 'tags', 'videos', 'videos.thumbnail']);
        $q->skip($request->get('skip', 0))->take($request->get('take', 10));
        if ($request->input('search')) {
            $q->where('name', 'like', sprintf('%%%s%%', $request->input('search')));
        }
        if ($request->input('deleted', 'false') !== 'false' && $request->user()->can('manage', Package::class)) {
            $q->withTrashed();
        }
        if ($request->input('tags')) {
            $q->select(DB::raw('packages.*, COUNT(taggables.tag_id) as tagmatch'));
            $q->join('taggables', 'packages.id' , '=', 'taggables.taggable_id');
            $q->whereIn('taggables.tag_id', $tags = explode(',', $request->input('tags')));
            $q->groupBy('packages.id');
        }
        $q->orderBy('created_at', 'desc');
        $packages = $q->get();
        if($request->input('tags')) {
            $packages = $packages->where('tagmatch', '>=', count($tags))->values();
        }
        return response()->json($packages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate($this->updateValidator());

        $validated['tags'] = collect($validated['tags'])->pluck('id');
        $validated['videos'] = collect($validated['videos'])->pluck('id');

        $package = new Package($validated);

        $package->save();

        $package->tags()->sync($validated['tags']);

        $package->videos()->sync($validated['videos']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        return response()->json($package->loadMissing('videos', 'tags', 'videos.thumbnail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        $validated = $request->validate($this->updateValidator());

        $validated['tags'] = collect($validated['tags'])->pluck('id');
        $validated['videos'] = collect($validated['videos'])->pluck('id');

        $package->update($validated);

        $package->tags()->sync($validated['tags']);

        $package->videos()->sync($validated['videos']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
    }

    public function forceDelete($package) {
        $pkg = Package::withTrashed()->find($package);

        $this->authorize('manage', $pkg);

        $pkg->forceDelete();
    }

    public function restore($package) {
        $pkg = Package::withTrashed()->find($package);

        $this->authorize('manage', $pkg);

        $pkg->restore();
    }
}
