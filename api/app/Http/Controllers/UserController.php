<?php

namespace App\Http\Controllers;

use App\User;
use App\Video;
use App\Package;
use App\GiftCard;
use App\Classes\Cloudflare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserController extends Controller
{
    public function __construct() {
        $this->authorizeResource(User::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->user() && $request->user()->can('admin')) {
            return response()->json(User::with('videos')->get());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Request $request)
    {
        if($request->user() && $request->user()->can('admin')) {
            $u = $user->loadMissing(['videos', 'cart'])->toArray();
            $u['stripe_id'] = $user->stripe_id;
            return response()->json($u);
        }
        return response('', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function videos(Request $request) {
        $user = $request->user();
        $user->loadMissing(['videos', 'videos.thumbnail', 'videos.tags', 'videos.duration_tag']);
        $myvideos = $user->videos->sortBy('pivot.expires')->values()->keyBy('id');

        $pids = DB::table('package_video')->whereIn('video_id', $myvideos->map->id)->get()->pluck('package_id')->unique();
        $allpackages = Package::whereIn('id', $pids)->with('videos')->get();

        $videos = $myvideos;
        $packages = collect();
        foreach($allpackages as $package) {
            $v = $myvideos->whereIn('id', $vids = $package->videos->map->id);
            if (count($v) === count($package->videos)) {
                $videos = $videos->except($vids->toArray());
                $p = (object)$package->toArray();
                $p->videos = collect($p->videos)->map(function(&$el) use($myvideos) { return $myvideos[$el['id']]; })->toArray();
                $p->lowestExp = $v->sortBy('pivot.expires')->values()[0]->pivot->expires;
                $packages[] = $p;
            }
        }
        $packages = $packages->sortByDesc('lowestExp')->values();
        $videos = $videos->sortByDesc('pivot.expires')->values();
        return response()->json(['videos' => $videos, 'packages' => $packages], 200);
    }

    public function getCurrent(Request $request) {
        return response()->json($request->user()->loadMissing(['roles', 'roles.abilities', 'abilities', 'cart', 'cart.thumbnail', 'videos', 'promo']));
    }

    public function getCart(Request $request) {
        return response()->json($request->user()->loadMissing(['cart', 'cart.thumbnail'])->cart);
    }

    public function addToCart(Request $request) {
        $u = $request->user();
        $u->update(['has_seen_cart_warning' => true]);
        $id = $request->input('id');

        switch($request->input('type')) {
            case 'video':
                $cartables = collect([Video::find($id)]); break;
            case 'package':
                $cartables = Package::find($id)->videos; break;
        }

        $cartables = $cartables->keyBy('id')->filter(function($x) { return $x->price > 0; })->map(function($el) { return ['added_price' => $el->price]; });

        $cartables = $cartables->diffKeys($u->cart->keyBy('id'))->diffKeys($u->videos->where('pivot.expires', '>', Carbon::now())->keyBy('id'));

        if(!$cartables) {
            return response()->error('Product not found', 404);
        }

        $u->cart()->attach($cartables);

        $u->refresh();

        return response()->json($u->loadMissing(['cart', 'cart.thumbnail'])->cart, 201);
    }

    public function removeFromCart(Request $request, Video $video) {
        $u = $request->user();
        
        $u->cart()->detach($video);

        return response()->json($u->loadMissing(['cart', 'cart.thumbnail'])->cart, 200);
    }

    public function upload(Request $request) {
        $this->authorize('admin');

        return response()->json(Cloudflare::getUploadUrl());
    }

    public function redeem(Request $request) {
        $user = $request->user();

        $giftcards = GiftCard::where('redemption', $request->input('code'))->get();

        if (!count($giftcards) || $giftcards[0]->redeemed_by_id) {
            return response('', 404);
        }

        $giftcards[0]->update(['redeemed_by_id' => $user->id]);

        $user->balance += $giftcards[0]->value;
        $user->save();

        return response('', 200);
    }

    public function newInLibrary(Request $request) {
        $user = $request->user();

        return $user->videos()->with('thumbnail')->get()->sortBy('pivot.expires')->take(4)->where('pivot.expires', '>=', Carbon::now());
    }

    public function createPortalSession(Request $request) {
        $user = $request->user();
        $session = \Stripe\BillingPortal\Session::create([
            'customer' => $user->createOrGetStripeCustomer(),
            'return_url' => route('home', ['any' => 'app/home'])
        ]);

        return response()->json($session->url, 201);
    }
}
