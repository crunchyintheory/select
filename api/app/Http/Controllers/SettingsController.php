<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller {

    public $validator = [
        'site_name' => 'string',
        'package_name' => 'string',
        'package_name_plural' => 'string',
        'package_name_plural_listing' => 'string',
        'video_name' => 'string',
        'video_name_plural' => 'string',
        'video_name_plural_listing' => 'string',
        'login_page_text' => 'string',
        'home_page_text' => 'string',
        'org_name' => 'string',
        'org_logo' => 'string',
        'footer_left_text' => 'string',
        'footer_right_text' => 'string',
        'subscribe_page_text' => 'string',
        'manage_subscription_dialog_text' => 'string'
    ];
    
    public function index() {
        return Setting::select('name', 'value')->where('hidden', 0)->get();
    }

    public function update(Request $request) {

        $this->authorize('update', Setting::class);
        
        $validated = $request->validate($this->validator);

        foreach($validated as $name => $value) {
            Setting::where('name', $name)->update(['value' => $value]);
        }

        return response()->json($this->index());

    }

}