<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\Setting;
use Illuminate\Http\Request;
use Throwable;

class SubscriptionController extends Controller
{
    public function __construct() {
        $this->authorizeResource(Subscription::class);
    }

    /**
     * @return \Stripe\Product
     */
    public static function getSubscriptionProduct() {
        $prod = Setting::get('sub_product', true);
        if($prod) {
            try {
                $prod = \Stripe\Product::retrieve($prod);
            } catch (Throwable $e) {
                $prod = null;
            }
        }
        if(!$prod) {
            $prod = \Stripe\Product::create([
                'name' => 'Subscription'
            ]);

            Setting::updateOrCreate([
                'name' => 'sub_product'
            ], [
                'value' => $prod->id,
                'hidden' => true
            ]);
        }

        return $prod;
    }

    protected function storeValidator() {
        return [
            'name' => 'required|string|max:255',
            'price' => 'numeric|regex:/^\d+(?:\.\d{1,2})?$/',
            'credit' => 'numeric|regex:/^\d+(?:\.\d{1,2})?$/',
            'description' => 'nullable|string|max:1000',
        ];
    }

    protected function updateValidator() {
        return [
            'name' => 'required|string|max:255',
            'credit' => 'numeric|regex:/^\d+(?:\.\d{1,2})?$/',
            'description' => 'nullable|string|max:1000',
        ];
    }

    protected function resourceAbilityMap() {
        return [
            // 'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Subscription::query()->orderBy('price', 'asc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate($this->storeValidator());

        $data['price'] = floor(floatval($data['price'] * 100));
        $data['credit'] = floor(floatval($data['credit'] * 100));
        
        $stripe = \Stripe\Price::create([
            'product' => static::getSubscriptionProduct()->id,
            'currency' => 'USD',
            'unit_amount' => $data['price'],
            'active' => true,
            'nickname' => $data['name'],
            'recurring' => ['interval' => 'month']
        ]);

        $data['price_id'] = $stripe->id;

        $subscription = Subscription::create($data);

        return response()->json($subscription, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        return response()->json($subscription);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        $data = $request->validate($this->updateValidator());
        $data['credit'] = floor(floatval($data['credit'] * 100));

        $subscription->update($data);

        return response()->json($subscription);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        \Stripe\Price::update($subscription->price_id, [
            'active' => false
        ]);
        $subscription->delete();
    }

    public function checkout(Subscription $subscription, Request $request) {
        /**
         * @var \Stripe\Customer $customer
         */
        $customer = $request->user()->createOrGetStripeCustomer();

        $subs = \Stripe\Subscription::all(['customer' => $customer->id, 'status' => 'active']);
        if($subs->count()) {
            throw 'already_subscribed';
        }

        $intents = collect(\Stripe\PaymentIntent::all(['customer' => $customer->id])->data)->whereIn('status', ['requires_payment_method', 'requires_capture', 'requires_confirmation', 'requires_action']);
        foreach($intents as $intent) {
            /**
             * @var \Stripe\PaymentIntent $intent
             */
            try {
                $intent->cancel(['cancellation_reason' => 'abandoned']);
            } catch (Throwable $e) {}
        }

        $session = \Stripe\Checkout\Session::create([
            'customer' => $customer,
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price' => $subscription->price_id,
                'quantity' => 1
            ]],
            'mode' => 'subscription',
            'success_url' => route('payment.complete') . '/{CHECKOUT_SESSION_ID}',
            'cancel_url' => route('home', ['any' => 'app/subscribe'])
        ]);

        return response()->json($session->id, 201);
    }
}
