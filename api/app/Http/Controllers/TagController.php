<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    public function __construct() {
        $this->authorizeResource(Tag::class);
    }

    protected function resourceAbilityMap() {
        return [
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete'
        ];
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tags = Tag::query();
        if ($request->input('duration') === 'false') {
            $tags->where('time_threshold', null);
        }
        if ($type = $request->input('type')) {
            $tags->select(DB::raw('tags.*, COUNT(taggables.taggable_id) as tagmatch'));
            $tags->leftJoin('taggables', 'taggables.tag_id', '=', 'tags.id');
            $tags->where(function($q) use ($type) {
                if ($type === 'videos') {
                    $q->where('taggables.taggable_type', '=', 'App\\Video')->orWhere('tags.time_threshold', '!=', null);
                } else if ($type === 'packages') {
                    $q->where('taggables.taggable_type', '=', 'App\\Package');
                }
            });
            $tags->groupBy('tags.id');
            $tags->orderBy('tags.name')->orderBy('tags.time_threshold');
        }

        return response()->json($tags->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = Tag::firstOrCreate(['name' => $request->input('name')]);

        return response()->json($tag);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        //
    }
}
