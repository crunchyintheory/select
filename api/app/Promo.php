<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * @property string $code
 * @property \Carbon\Carbon $expires
 * @property integer $discount
 */
class Promo extends Model
{
    public $fillable = [
        'code',
        'expires',
        'discount'
    ];

    public $casts = [
        'expires' => 'datetime',
        'discount' => 'integer'
    ];

    protected static function booted() {
        static::addGlobalScope('valid', function (Builder $builder) {
            if(!Auth::user() || !Auth::user()->can('manage', Promo::class)) {
                $builder->where('expires', '>', Carbon::now());
            }
        });
    }
}
