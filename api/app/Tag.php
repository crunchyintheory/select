<?php

namespace App;

use App\Video;
use App\Package;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property integer $time_threshold
 * @property-read \Illuminate\Support\Collection $videos
 * @property-read \Illuminate\Support\Collection $packages
 */
class Tag extends Model
{
    protected $fillable = [
        'name',
        'time_threshold'
    ];

    protected $casts = [
        'time_threshold' => 'integer'
    ];

    public function videos() {
        return $this->morphedByMany(Video::class, 'taggable');
    }

    public function packages() {
        return $this->morphedByMany(Package::class, 'taggable');
    }
}
