<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class GenerateCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'select:customers {user?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate customer IDs for all unassigned users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($id = $this->argument('user')) {
            $users = collect($u = User::find($id));
            if(!$u) {
                $this->error('Could not find user ' . $id);
                exit;
            }
        } else {
            $users = User::query()->withTrashed()
                ->where('stripe_id', null)
                ->orWhere('stripe_id', '')
                ->get();

            if(!$users->count()) {
                $this->comment('No unassigned users found. Exiting.');
                exit;
            }
        }

        
        $resp = $this->confirm("{$users->count()} customers will be generated. Are you sure you want to continue?");

        if(!$resp) {
            exit;
        }

        foreach($users as $user) {

            $user->createOrGetStripeCustomer();

            $this->info('Generated customer for ' . $user->id);
            $this->line('');

            sleep(1);
        }
    }
}
