<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Video;

class GenerateProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'select:products {video?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate product IDs for all unassigned videos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($id = $this->argument('video')) {
            $videos = collect($v = Video::find($id));
            if(!$v) {
                $this->error('Could not find video ' . $id);
                exit;
            }
        } else {
            $videos = Video::withoutGlobalScope('published')->withTrashed()
                ->where('product_id', null)
                ->orWhere('product_id', '')
                ->get();

            if(!$videos->count()) {
                $this->comment('No unassigned videos found. Exiting.');
                exit;
            }
        }

        
        $resp = $this->confirm("{$videos->count()} products will be generated. Are you sure you want to continue?");

        if(!$resp) {
            exit;
        }

        foreach($videos as $video) {
            
            $product = \Stripe\Product::create([
                'name' => $video->name,
                'description' => '72 Hours to start watching, 24 hours to finish watching after you start.',
                'metadata' => [
                    'video_id' => $video->id
                ]
            ]);

            $video->update(['product_id' => $product->id]);

            $this->info('Generated product for ' . $video->id);
            $this->line('');

            sleep(1);
        }
    }
}
