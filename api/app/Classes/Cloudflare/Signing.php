<?php

namespace App\Classes\Cloudflare;

use App\Video;
use App\User;
use App\Setting;
use Firebase\JWT\JWT;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class Signing {    

    public static function getKey() {
        if ($s = Setting::get('cfkey', true)) {
            return json_decode($s);
        } else {

            $res = static::newKey();
            
            (new Setting([
                'name' => 'cfkey',
                'value' => json_encode($value = (object)[
                    'pem' => $res->pem,
                    'jwt' => $res->jwk,
                    'cfid' => $res->id,
                ]),
                'hidden' => 1
            ]))->save();

            return $value;

        }
    }

    private static function revokeKey(object $key) {
        return Http::withToken(config('services.cloudflare.key'))
            ->delete(sprintf('https://api.cloudflare.com/client/v4/accounts/%s/media/keys/%s', config('services.cloudflare.account'), $key->cfid));
    }

    private static function newKey() {
        $res = Http::withToken(config('services.cloudflare.key'))
            ->post(sprintf('https://api.cloudflare.com/client/v4/accounts/%s/media/keys', config('services.cloudflare.account')));

        $res->throw();

        return $res->object()->result;
    }

    public static function sign(Video $video, Carbon $exp) {
        $key = static::getKey();

        return JWT::encode(
            array(
                'id' => $key->cfid,
                'kid' => $key->cfid,
                'sub' => $video->videouid,
                'exp' => $exp->timestamp
            ),
            base64_decode($key->pem),
            'RS256'
        );
    }
}