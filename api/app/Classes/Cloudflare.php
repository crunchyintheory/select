<?php

namespace App\Classes;

use App\Video;
use App\User;
use Carbon\Carbon;

class Cloudflare {

    public static function getUploadUrl() {
        /*$expiry = Carbon::now()->add(6, 'hours')->sub(1, 'minute')->toRfc3339String();

        $data = array(
            'maxDurationSeconds' => 7200,
            'expiry' => $expiry,
            'requireSignedURLs' => true,
            'allowedOrigins' => config('sanctum.stateful')
        );

        $ch = curl_init(sprintf('https://api.cloudflare.com/client/v4/accounts/%s/media/direct_upload', config('services.cloudflare.account')));

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . config('services.cloudflare.key')
        ));

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

        $res = curl_exec($ch);

        if(!$res || !json_decode($res)->success) {
            throw 'Could not get upload URL';
        }

        return json_encode(array(
            'url' => json_decode($res)->result->uploadURL,
            'headers' => array()
        ));*/ // Direct uploads don't work with TUS

        return json_encode(array(
            'url' => sprintf('https://api.cloudflare.com/client/v4/accounts/%s/stream', config('services.cloudflare.account')),
            'headers' => array(
                'Authorization' => sprintf('Bearer %s', config('services.cloudflare.key'))
            )
        ));
    }

    public static function sign(Video $video, User $user, Carbon $exp) {
        return Cloudflare\Signing::sign($video, $exp);
    }

}