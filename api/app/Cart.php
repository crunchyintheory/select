<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $user_id
 * @property User $user
 * @property string $video_id
 * @property integer $added_price
 * @property integer $promo_id
 */
class Cart extends Model
{
    protected $fillable = [
        'user_id',
        'video_id',
        'added_price',
        'promo_id'
    ];

    protected $casts = [
        'added_price' => 'integer'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function video() {
        return $this->belongsTo('App\Video');
    }
}
