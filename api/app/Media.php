<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $filename
 * @property string $uploader_id
 * @property-read User $uploader
 * @property string $mediable_type
 * @property string $mediable_id
 * @property-read mixed $mediable
 * @property-read string $url
 */
class Media extends Model
{
    use UsesUuid;

    protected $fillable = [
        'id', 
        'filename',
        'uploader_id',
        'mediable_type',
        'mediable_id'
    ];

    protected $appends = [
        'url'
    ];

    public function getUrlAttribute() {
        return asset($this->filename);
    }

    public function uploader() {
        return $this->belongsTo('App\User', 'uploader_id');
    }

    public function mediable() {
        return $this->morphTo();
    }

}
