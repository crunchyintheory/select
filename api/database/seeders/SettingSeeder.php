<?php

namespace Database\Seeders;

use App\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\BouncerFacade as Bouncer;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::firstOrCreate([
            'name' => 'site_name'
        ], [
            'value' => 'Select'
        ]);

        Setting::firstOrCreate([
            'name' => 'package_name'
        ], [
            'value' => 'Package'
        ]);

        Setting::firstOrCreate([
            'name' => 'package_name_plural'
        ], [
            'value' => 'Packages'
        ]);

        Setting::firstOrCreate([
            'name' => 'package_name_plural_listing'
        ], [
            'value' => 'Packages'
        ]);

        Setting::firstOrCreate([
            'name' => 'video_name'
        ], [
            'value' => 'Video'
        ]);

        Setting::firstOrCreate([
            'name' => 'video_name_plural'
        ], [
            'value' => 'Videos'
        ]);

        Setting::firstOrCreate([
            'name' => 'video_name_plural_listing'
        ], [
            'value' => 'All Videos'
        ]);

        Setting::firstOrCreate([
            'name' => 'login_page_text'
        ], [
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut convallis lacus, id placerat justo. Fusce sed finibus ipsum. Sed bibendum blandit metus in congue. Maecenas risus mi, bibendum ac massa sit amet, malesuada malesuada ex. Morbi convallis lectus sit amet enim facilisis, vitae sagittis neque condimentum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi vel iaculis metus. Vivamus convallis consequat mauris et pharetra. Phasellus vel nibh id sem consectetur consectetur sit amet id felis. Quisque ipsum ligula, commodo quis euismod ut, rutrum at massa. Morbi bibendum tempus nunc ac tincidunt. Donec in felis condimentum, finibus dolor sed, consequat purus. Etiam luctus ex mauris, sed dignissim nulla porttitor ut. Nunc dolor felis, molestie consectetur turpis et, fringilla faucibus sapien. Suspendisse sit amet massa tincidunt, dapibus turpis quis, dapibus ligula. Nulla scelerisque dignissim volutpat. Nam eget volutpat nisi. Nunc hendrerit lacus nec mauris molestie, ac varius leo facilisis.'
        ]);

        Setting::firstOrCreate([
            'name' => 'org_name'
        ], [
            'value' => 'Select'
        ]);

        Setting::firstOrCreate([
            'name' => 'org_logo'
        ], [
            'value' => '/favicon.ico'
        ]);

        Setting::firstOrCreate([
            'name' => 'home_page_text'
        ], [
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porttitor neque in velit malesuada, posuere vestibulum orci laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec auctor eros diam, nec euismod tellus volutpat non. Vivamus fermentum id neque vulputate porttitor. Duis efficitur libero a ipsum lobortis facilisis. Pellentesque placerat diam metus, sed tincidunt dolor lobortis vitae. Suspendisse id ipsum a lectus suscipit facilisis vitae vitae leo. Etiam libero libero, hendrerit id feugiat non, semper sed velit. Aenean sed risus non nunc consequat luctus rutrum vel erat. Morbi a feugiat elit. Vestibulum consequat ipsum in arcu euismod, in faucibus ipsum elementum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae'
        ]);

        Setting::firstOrCreate([
            'name' => 'footer_left_text'
        ], [
            'value' => '*Left Footer Text*'
        ]);

        Setting::firstOrCreate([
            'name' => 'footer_right_text'
        ], [
            'value' => '*Right Footer Text*'
        ]);

        Setting::firstOrCreate([
            'name' => 'subscribe_page_text'
        ], [
            'value' => '*Subscribe Page Text*'
        ]);

        Setting::firstOrCreate([
            'name' => 'manage_subscription_dialog_text'
        ], [
            'value' => 'You will be taken to Stripe, where you can cancel your subscription or update your payment method.'
        ]);

        $cfkey = Setting::where('name', 'cfkey')->get();

        if(isset($cfkey) && $cfkey->has(0)) {
            $cfkey[0]->update(['hidden' => 1]);
        }
    }
}
