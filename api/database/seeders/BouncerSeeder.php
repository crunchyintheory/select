<?php

namespace Database\Seeders;

use App\User;
use App\Video;
use App\Package;
use App\Media;
use App\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\BouncerFacade as Bouncer;

class BouncerSeeder extends Seeder
{
    protected $publicClasses = [
        Video::class,
        Package::class,
        Media::class,
        Tag::class
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::role()->firstOrCreate([
            'name' => 'admin',
            'title' => 'Administrator'
        ]);
        
        Bouncer::allow('admin')->everything();

        Bouncer::allow('admin')->to('admin');

        Bouncer::role()->firstOrCreate([
            'name' => 'user',
            'title' => 'Default User'
        ]);

        Bouncer::allow('user')->toOwn(User::class);
        Bouncer::forbid('user')->to('create', User::class);

        foreach($this->publicClasses as $class) {
            Bouncer::allow('user')->to(['view', 'viewAny'], $class);
        }

        Bouncer::allow('user')->toOwn(Video::class)->to(['watch']);
    }
}
