<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Tag;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach([15, 30, 45, 60] as $time) {
            Tag::firstOrCreate(['time_threshold' => $time * 60]);
        }
    }
}
