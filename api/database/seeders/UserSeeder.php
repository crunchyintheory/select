<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\BouncerFacade as Bouncer;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DEV', '0') === '1') {
            $admin = User::firstOrCreate([
                'first_name' => 'administrator',
                'last_name' => 'Lastname',
                'email' => 'test@removemeplease.com',
            ], [
                'email_verified_at' => \Carbon\Carbon::now(),
                'password' => Hash::make('password')
            ]);
    
            Bouncer::assign('admin')->to($admin);
        }
    }
}
