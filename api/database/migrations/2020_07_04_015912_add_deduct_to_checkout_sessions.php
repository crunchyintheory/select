<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeductToCheckoutSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkout_sessions', function (Blueprint $table) {
            $table->boolean('failed')->default(false);
            $table->integer('deduct')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkout_sessions', function (Blueprint $table) {
            $table->dropColumn('deduct', 'failed');
        });
    }
}
