<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGiftCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gift_cards', function (Blueprint $table) {
            $table->id();
            $table->string('redemption')->nullable()->unique();
            $table->string('name');
            $table->uuid('redeemed_by_id')->nullable();
            $table->integer('value');
            $table->timestamps(6);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('balance')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gift_cards');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('balance');
        });
    }
}
