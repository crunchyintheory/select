<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps(6);
            $table->string('name');
            $table->longText('description')->nullable();
            $table->bigInteger('duration');
            $table->enum('status', ['published', 'draft'])->default('published');
            $table->string('videouid');
            $table->string('cfstate')->default('unuploaded');
            
            $table->uuid('uploader_id');

            $table->integer('duration_tag_id')->nullable();

            $table->softDeletes('deleted_at', 6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
