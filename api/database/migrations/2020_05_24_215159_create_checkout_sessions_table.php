<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckoutSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_sessions', function (Blueprint $table) {
            $table->id();
            
            $table->string('stripe_id');
            $table->uuid('user_id');
            $table->integer('subscription_id')->nullable();
            $table->boolean('fulfilled')->default(false);

            $table->timestamps(6);
        });

        Schema::create('checkout_session_video', function (Blueprint $table) {
            $table->id();

            $table->integer('checkout_session_id');
            $table->uuid('video_id');

            $table->timestamp('expires', 6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout_sessions');
        Schema::dropIfExists('checkout_session_video');
    }
}
