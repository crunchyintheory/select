<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SplitUserName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('name', 'first_name');
            $table->string('last_name');
        });

        User::all()->each(function($user) {
            $names = explode(' ', $user->first_name, 2);
            $user->update([
                'first_name' => $names[0],
                'last_name' => $names[1] ?? 'Lastname'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::all()->each(function($user) {
            $user->update([
                'first_name' => $user->first_name . ' ' . $user->last_name
            ]);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_name');
            $table->renameColumn('first_name', 'name');
        });
    }
}
