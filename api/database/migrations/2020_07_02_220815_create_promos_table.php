<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->integer('discount');
            $table->timestamp('expires');
            $table->integer('usage_count')->default(0);
            $table->timestamps(6);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('promo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
        
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('promo_id');
        });
    }
}
