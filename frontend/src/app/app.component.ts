import { Component } from '@angular/core';
import { StripeService } from './services/stripe.service';
import { UserService } from './api/models/user';
import { SettingService } from './api/models/setting';
import { Router, ActivationEnd, ActivatedRouteSnapshot } from '@angular/router';
import { filter } from 'rxjs/operators';
import { TitleService } from './services/title.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private static RecurseRouteConfig(route: ActivatedRouteSnapshot): string {
    return route.data.title as string || (route.parent && AppComponent.RecurseRouteConfig(route.parent));
  }

  constructor(public stripe: StripeService, public user: UserService, public settings: SettingService, public title: TitleService, private router: Router) { // Load stripe service on every page
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    this.user.current().subscribe(() => { }, () => {
      this.user.signedOut = true;
      this.user.user_or_loggedout.next(false);
    });
    this.settings.list().subscribe();
    this.router.events.pipe(
      filter(x => x instanceof ActivationEnd && x.snapshot.component !== undefined)
    ).subscribe((x: ActivationEnd) => {
      const title: string = AppComponent.RecurseRouteConfig(x.snapshot);
      if(title !== 'hold') {
        this.title.setTitle(title);
      } 
    });
    this.title.setTitle();
  }
}
