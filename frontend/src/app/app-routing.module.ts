import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule, CanActivate, ActivatedRouteSnapshot, CanActivateChild, Router } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/login/register.component';
import { UploadComponent } from './components/videos/upload/upload.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ListVideosComponent } from './components/videos/list/list.component';
import { SingleVideoComponent } from './components/videos/single/single.component';
import { HomeComponent } from './components/home/home.component';
import { ListPackagesComponent } from './components/packages/list/list.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ManagePackagesComponent } from './components/packages/manage/manage.component';
import { ManageVideosComponent } from './components/videos/manage/manage.component';
import { EditVideoComponent } from './components/videos/edit/edit.component';
import { EditPackageComponent } from './components/packages/edit/edit.component';
import { LibraryComponent } from './components/videos/library/library.component';
import { WatchVideoComponent } from './components/videos/watch/watch.component';
import { PaymentCompleteComponent } from './components/payment/complete/complete.component';
import { SettingsComponent } from './components/dashboard/settings/settings.component';
import { UserService } from './api/models/user';
import { first } from 'rxjs/operators';
import { RedeemComponent } from './components/redeem/redeem.component';
import { ManagePromosComponent } from './components/dashboard/promos/manage/manage.component';
import { ManageGiftCardsComponent } from './components/dashboard/gift-cards/manage/manage.component';
import { EditPromoComponent } from './components/dashboard/promos/edit/edit.component';
import { EditGiftCardComponent } from './components/dashboard/gift-cards/edit/edit.component';
import { HelpComponent } from './components/help/help.component';
import { ForgotPasswordComponent } from './components/login/reset/reset.component';
import { ManageUsersComponent } from './components/users/manage/manage.component';
import { EditUserComponent } from './components/users/edit/edit.component';
import { SubscribeComponent } from './components/subscription/subscribe/subscribe.component';
import { ManageSubscriptionsComponent } from './components/subscription/manage/manage.component';

@Injectable()
export class CanActivateRole implements CanActivate, CanActivateChild {
  constructor(private user: UserService, public router: Router) {}

  public canActivate(route: ActivatedRouteSnapshot) {
    if (!route.data.permission) {
      return true;
    }

    return new Promise<boolean>(resolve => {
      this.user.user_or_loggedout.pipe(first(x => x !== null, false)).subscribe(user => {
        if (user === false) {
          this.router.navigate(['/home']);
          resolve(false);
        }
        
        //eslint-disable-next-line @typescript-eslint/no-misused-promises
        this.user.can(route.data.permission).subscribe(x => {
          if (!x) {
            this.router.navigate(['/home']);
          }
          resolve(x);
        });
      });      
    });
  }

  public canActivateChild(route: ActivatedRouteSnapshot) {
    let permission: string = null;
    let parent: ActivatedRouteSnapshot = route;
    do {
      if (parent.data.permission) {
        permission = parent.data.permission as string;
        break;
      }
      parent = parent.parent;
    } while (parent);

    if (!permission) {
      return true;
    }

    if (!this.user.signedIn) {
      return false;
    }

    return new Promise<boolean>(resolve => {
      this.user.user_or_loggedout.pipe(first(x => x !== null, false)).subscribe(user => {
        if (user === false) {
          this.router.navigate(['/home']);
          resolve(false);
        }
        
        //eslint-disable-next-line @typescript-eslint/no-misused-promises
        this.user.can(permission).subscribe(x => {
          if (!x) {
            this.router.navigate(['/home']);
          }
          resolve(x);
        });
      });      
    });
  }
}

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', children: [
    { path: '', component: LoginComponent, data: { title: 'Login' } },
    { path: 'reset', component: ForgotPasswordComponent, data: { title: 'Forgot Password' } }
  ] },
  { path: 'register', component: RegisterComponent, data: { title: 'Register' } },
  { path: 'videos', children: [
    { path: '', component: ListVideosComponent, data: { title: '~video_name_plural_listing' } },
    { path: 'library', component: LibraryComponent, data: { title: 'My Library' } },
    { path: 'upload', component: UploadComponent, data: { title: 'Upload', permission: 'admin' }, canActivate: [CanActivateRole] },
    { path: ':id', children: [
      { path: '', component: SingleVideoComponent, data: { title: 'hold' } },
      { path: 'watch', component: WatchVideoComponent, data: { title: 'hold' } }
    ] }
  ] },
  { path: 'packages', children: [
    { path: '', component: ListPackagesComponent, data: { title: '~package_name_plural_listing' } }
  ] },
  { path: 'dashboard', component: DashboardComponent, children: [
    { path: '', redirectTo: 'settings', pathMatch: 'full' },
    { path: 'settings', component: SettingsComponent },
    { path: 'promos', children: [
      { path: '', component: ManagePromosComponent },
      { path: 'create', component: EditPromoComponent },
      { path: ':id/edit', component: EditPromoComponent }
    ] },
    { path: 'gift-cards', children: [
      { path: '', component: ManageGiftCardsComponent },
      { path: 'create', component: EditGiftCardComponent },
      { path: ':id/edit', component: EditGiftCardComponent }
    ] },
    { path: 'videos', children: [
      { path: '', component: ManageVideosComponent },
      { path: ':id/edit', component: EditVideoComponent }
    ] },
    { path: 'packages', children: [
      { path: '', component: ManagePackagesComponent },
      { path: 'create', component: EditPackageComponent },
      { path: ':id/edit', component: EditPackageComponent }
    ] },
    { path: 'upload', component: UploadComponent },
    { path: 'users', children: [
      { path: '', component: ManageUsersComponent },
      { path: ':id/edit', component: EditUserComponent }
    ] },
    { path: 'subscriptions', children: [
      { path: '', component: ManageSubscriptionsComponent }
    ] }
  ], data: { title: 'Dashboard', permission: 'admin' }, canActivate: [CanActivateRole] },
  { path: 'checkout', component: CheckoutComponent, data: { title: 'Cart' } },
  { path: 'payment', children: [
    { path: 'complete/:session', component: PaymentCompleteComponent, data: { title: 'Payment Complete' } }
  ]},
  { path: 'redeem', component: RedeemComponent, data: { title: 'Redeem' } },
  { path: 'help/:page', component: HelpComponent, data: { title: 'Help' } },
  { path: 'subscribe', component: SubscribeComponent, data: { title: 'Subscribe' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [CanActivateRole]
})
export class AppRoutingModule { }
