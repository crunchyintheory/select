import { HttpHeaders, HttpParams } from '@angular/common/http';
import { from } from 'rxjs';
import { reduce, map } from 'rxjs/operators';

// tslint:disable-next-line: max-line-length
export interface HttpClientOptions {
    headers?: HttpHeaders | { [header: string]: string | string[]; };
    params?: HttpParams | { [param: string]: string | string[]; };
    reportProgress?: boolean;
    withCredentials?: boolean;
}

export class RequestBuilderOptions implements HttpClientOptions {
    headers = new HttpHeaders();
    params = new HttpParams();
    parameters = new Map<string, string>();
    reportProgress = false;
    withCredentials = false;

    public queryString() {
        return from(this.parameters).pipe(
            reduce((acc, [key, value]) => acc + `${key}=${value}&`, '?'),
            map(x => x.slice(0, -1))
        );
    }
}
