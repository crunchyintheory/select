import { Model } from "../model";
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SettingService {
    
    public _settings = {};
    public settings = new BehaviorSubject<{ [key: string]: string }>(this._settings);

    constructor(public data: DataService) {}

    private map = (x: Setting[]) => {
        for(const setting of x) {
            this._settings[setting.name] = setting.value;
        }

        this.settings.next(this._settings);
    };

    public list() {
        return this.data.get<Setting[]>('/settings').pipe(tap(this.map));
    }

    public update(data: { [key: string]: string }) {
        return this.data.put<{ [key: string]: string }, Setting[]>('/settings', data).pipe(tap(this.map));
    }

}

export class Setting extends Model {
    public name: string;
    public value: string;
}