import { Injectable } from '@angular/core';
import { UuidModel } from '../model';
import { SearchableModelService } from '../model.service';
import { UploadOptions, Upload, PreviousUpload } from 'tus-js-client';
import { from, BehaviorSubject, Subject } from 'rxjs';
import { RequestBuilder } from '../RequestBuilder';
import { User } from './user';
import { Media } from './media';
import { Tag } from './tag';
import { Record } from './record';

@Injectable({
  providedIn: 'root'
})
export class VideoService extends SearchableModelService<Video> {
  public route = 'videos';

  private promptResume(previous: PreviousUpload[]): PreviousUpload {
    if (previous.length === 0) {
      return null;
    }

    let text = 'You tried to upload this file previously at these times:\n\n';
    previous.forEach((previousUpload, i) => {
      text += `[${i}]${previousUpload.creationTime}\n`;
    });
    text += '\nEnter the corresponding number to resume an upload or press Cancel to start a new upload';

    const answer = prompt(text);
    const index = parseInt(answer, 10);

    if (!isNaN(index) && previous[index]) {
      return previous[index];
    }
  }

  protected finalizeVideoUpload(video: Video): RequestBuilder<Video> {
    return this.store<Video>(video);
  }

  // tslint:disable-next-line: max-line-length
  public upload(endpoint: string, headers: { [key: string]: string }, file: File, preSave: (videouid: string) => Video = null, complete: Subject<Video> = null, error: Subject<string> = null, status: Subject<'error' | 'progress' | 'complete'> = null) {
    const subject = new BehaviorSubject<number>(0);
    const options: UploadOptions = {
      endpoint,
      headers,
      chunkSize: 5 * 1024 * 1024, // 5 MB
      metadata: {
        filename: `Select: ${file.name}`,
        filetype: file.type,
        name: `Select: ${file.name}`,
        requireSignedURLs: 'true'
      },
      uploadSize: file.size,
      removeFingerprintOnSuccess: true,
      onError: err => {
        upload.abort(true);
        if (status) {
          status.next('error');
        }
        if (error) {
          error.next(err.message);
        } else {
          throw err;
        }
      },
      onProgress: (bytesUploaded, bytesTotal) => {
        const percentage = (bytesUploaded / bytesTotal * 100);
        subject.next(percentage);
      },
      onSuccess: () => {
        subject.next(100);
        console.log('Upload finished:', upload.url);
        const videouid = upload.url.substr(upload.url.lastIndexOf('/') + 1).split('?')[0];
        const video = preSave ? preSave(videouid) : new Video({ videouid, name: file.name });
        this.finalizeVideoUpload(video).subscribe(x => {
          if (complete) {
            complete.next(x);
          }
          if (status) {
            status.next('complete');
          }
          subject.complete();
        });
      }
    };

    const upload = new Upload(file, options);

    from(upload.findPreviousUploads()).subscribe(previousUploads => {
      const chosenUpload = this.promptResume(previousUploads);

      if (chosenUpload) {
          upload.resumeFromPreviousUpload(chosenUpload);
      }

      upload.start();
    });

    return subject.asObservable();
  }

  public watch(video: string | Video): RequestBuilder<{ token: string }> {
    if (typeof video === 'object') {
      video = video.id;
    }

    return this.data.get(`/${this.route}/${video}/watch`);
  }

  public canWatch(video: string | Video): RequestBuilder<boolean> {
    if (typeof video === 'object') {
      video = video.id;
    }

    return this.data.get(`/${this.route}/${video}/canWatch`, false);
  }

  public featured(): RequestBuilder<Video> {
    return this.data.get(`/${this.route}/featured`);
  }

  public forceDelete(video: string): RequestBuilder<never> {
    return this.data.delete(`/${this.route}/${video}/force`);
  }

  public restore(video: string): RequestBuilder<never> {
    return this.data.post(`/${this.route}/${video}/restore`, {});
  }

  public setFeatured(video: string): RequestBuilder<Video> {
    return this.data.post(`/${this.route}/featured`, { video });
  }
}

export class Video extends UuidModel {
  name: string;
  description?: string;
  duration?: number;
  status?: 'published' | 'draft' | 'removed';
  videouid?: string;
  price?: number;
  cfstate?: string;

  // tslint:disable no-any
  uploader?: User;
  tags?: Array<Tag>;
  // tslint:disable-next-line: variable-name
  duration_tag?: Tag;
  thumbnail?: Media;
  // tslint:enable no-any

  deleted_at?: string;

  records?: Record[];
}

export class ExpiringVideo extends Video {
  pivot: {
    user_id: string;
    video_id: string;
    expires: string;
  }
}