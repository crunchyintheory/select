import { Injectable } from '@angular/core';
import { UuidModel } from '../model';
import { SearchableModelService } from '../model.service';

@Injectable({
  providedIn: 'root'
})
export class PromoService extends SearchableModelService<Promo> {
  public route = 'promos';
}

export class Promo extends UuidModel {
    code: string;
    expires: Date;
    discount: number;
}
