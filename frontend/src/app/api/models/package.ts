import { Injectable } from '@angular/core';
import { UuidModel } from '../model';
import { SearchableModelService } from '../model.service';
import { Media } from './media';
import { Tag } from './tag';
import { Video } from './video';
import { RequestBuilder } from '../RequestBuilder';

@Injectable({
  providedIn: 'root'
})
export class PackageService extends SearchableModelService<Package> {
  public route = 'packages';

  public forceDelete(pkg: string): RequestBuilder<never> {
    return this.data.delete(`/${this.route}/${pkg}/force`);
  }

  public restore(pkg: string): RequestBuilder<never> {
    return this.data.post(`/${this.route}/${pkg}/restore`, {});
  }
}

export class Package extends UuidModel {
  name: string;
  description?: string;
  media?: Media;
  tags?: Array<Tag>;
  videos?: Array<Video>;
  bgcolor?: string;
  price?: number;
  deleted_at?: string;
}
