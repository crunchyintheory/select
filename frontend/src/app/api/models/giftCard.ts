import { Injectable } from '@angular/core';
import { UuidModel } from '../model';
import { SearchableModelService } from '../model.service';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class GiftCardService extends SearchableModelService<GiftCard> {
  public route = 'gift-cards';
}

export class GiftCard extends UuidModel {
  name: string;
  value: number;
  redemption: string;
  redeemed_by?: User;
}
