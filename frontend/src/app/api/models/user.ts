import { Injectable } from '@angular/core';
import { UuidModel } from '../model';
import { ModelService } from '../model.service';
import { Video, ExpiringVideo } from './video';
import { RequestBuilder } from '../RequestBuilder';
import { tap, map, pluck, skip } from 'rxjs/operators';
import { BehaviorSubject, Subscribable, Observable, Subject } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
import { PipeableSubscribable } from '../PipeableSubscribable';
import { Package } from './package';
import { GetRequestBuilder } from '../GetRequestBuilder';
import { Promo } from './promo';

export type Library = { packages: Array<Package>; videos: Array<ExpiringVideo> };

@Injectable({
    providedIn: 'root'
})
export class UserService extends ModelService<User> {
    public route = 'users';

    public user = new BehaviorSubject<User>(new User());

    public user_or_loggedout = new BehaviorSubject<User | boolean>(null); // Emits false if user not signed in

    public cart: Observable<Array<Video>>;

    public library: Observable<Array<ExpiringVideo>>;

    public abilities: Observable<Array<Ability>>;

    private hasUser = false;

    public signedIn = false;

    public signedOut = null;

    public constructor(public data: DataService) {
        super(data);
        this.cart = this.user.pipe(
            pluck('cart')
        );
        this.library = this.user.pipe(
            pluck('videos')
        );
        this.user.pipe(skip(1)).subscribe(x => this.user_or_loggedout.next(x));
    }

    public setUser(user: User) {
        this.user.next(user);
        this.signedIn = true;
        this.signedOut = false;
    }

    protected setCart(cart: Array<Video>) {
        const user = this.user.value;

        user.cart = cart;

        this.user.next(user);
    }

    protected setLibrary(library: Array<ExpiringVideo>) {
        const user = this.user.value;

        user.videos = library;

        this.user.next(user);
    }

    public addToCart(id: string, type: string): RequestBuilder<Array<Video>> {
        return this.data.put<{ id: string, type: string }, Array<Video>>('user/cart', { id, type }).pipe(tap(x => this.setCart(x)));
    }

    public removeFromCart(id: string): RequestBuilder<Array<Video>> {
        return this.data.delete<Array<Video>>('user/cart/' + id).pipe(tap(x => this.setCart(x)));
    }

    public current(force = false): PipeableSubscribable<User> {
        if (!this.hasUser || force) {
            this.hasUser = true;
            return this.data.get<User>('user', false).pipe(
                tap(x => this.setUser(x))
            );
        }

        return this.user;
    }

    public getLibrary(): GetRequestBuilder<Library> {
        return this.data.get<Library>('user/videos');
    }

    public inCart(id: string): Subscribable<boolean> {
        if (this.hasUser) {
            this.current().subscribe();
        }

        return this.cart.pipe(
            map(x => x.some(y => y.id === id))
        );
    }

    public packageInCart(pkg: Package): Subscribable<boolean> {
        if (this.hasUser) {
            this.current().subscribe();
        }

        return this.cart.pipe(
            map(x => pkg.videos.every(y => x.some(z => z.id === y.id)))
        );
    }

    public inLibrary(id: string): Subscribable<boolean> {
        if (this.hasUser) {
            this.current().subscribe();
        }

        return this.library.pipe(
            map(x => x.some(y => y.id === id && new Date(y.pivot.expires) > new Date()))
        );
    }

    public packageInLibrary(pkg: Package): Subscribable<boolean> {
        if (this.hasUser) {
            this.current().subscribe();
        }

        return this.library.pipe(
            map(x => pkg.videos.every(y => x.some(z => z.id === y.id && new Date(z.pivot.expires) > new Date())))
        );
    }

    public upload(): RequestBuilder<{ url: string, headers: { [key: string]: string }}> {
        return this.data.get<string>('user/upload').pipe(map(x => JSON.parse(x))); // eslint-disable-line @typescript-eslint/no-unsafe-return
    }

    public can(permission: string, flags?: { id?: string | number, type?: string, owned?: boolean }): Observable<boolean> {
        return this.user.pipe(
            map(x => x.abilities.concat(x.roles.map(y => y.abilities).reduce((acc, cv) => acc.concat(cv), []))),
            map(x => {
                if (x.find(y => y.name === 'admin')) {
                    return true;
                }
                x = x.filter(y => y.name === permission);
                if (!flags) {
                    return x.length > 1;
                }
                if (flags.id) {
                    x = x.filter(y => y.entity_id === flags.id);
                }
                if (flags.type) {
                    x = x.filter(y => y.entity_type === flags.type);
                }
                if (flags.owned) {
                    x = x.filter(y => y.only_owned === flags.owned);
                }
                return x.length > 1;
            })
        );
    }

    public logout() {
        this.data.post('/logout', {}).subscribe(() => {
            window.location.href = document.head.querySelector('base').href + '/login';
            this.setUser(new User());
        });
    }

    public redeem(code: string): Observable<boolean> {
        const o = new Subject<boolean>();
        this.data.post('/user/redeem', { code }).subscribe(
        () => o.next(true),
        () => o.next(false),
        () => o.complete());
        return o.asObservable();
    }

    public setPromo(promo: Promo) {
        const u = this.user.value;
        u.promo = promo;
        this.setUser(u);
    }

    public newInLibrary() {
        return this.data.get<ExpiringVideo[]>('/user/libnew');
    }

    public resendVerification() {
        return this.data.post<unknown, never>('/email/resend', {});
    }

    public sendPasswordResetEmail(email: string) {
        return this.data.post('/password/email', { email });
    }

    public createPortalSession() {
        return this.data.post<unknown, string>('/checkout/portal', {});
    }
}

// tslint:disable variable-name

export class Ability {
    id: number;
    name: string;
    title: string;
    entity_id: string | number;
    entity_type: string;
    only_owned: boolean;
}

export class User extends UuidModel {
    public first_name: string;
    public last_name: string;
    public email: string;
    public cart?: Array<Video> = [];
    public videos?: Array<ExpiringVideo> = [];
    public abilities?: Array<Ability> = [];
    public roles?: Array<{ abilities: Array<Ability> }> = [];
    public promo?: Promo;
    public balance: number;
    public email_verified_at: string;
    public has_seen_cart_warning: boolean;
    public subscribed: boolean;
    public stripe_id?: string;
}
