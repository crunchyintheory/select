import { Model } from '../model';
import { Video } from './video';
import { Package } from './package';
import { Injectable } from '@angular/core';
import { ModelService } from '../model.service';

@Injectable({
  providedIn: 'root'
})
export class TagService extends ModelService<Tag> {
  public route = 'tags';
}

export class Tag extends Model {
  name: string;
  // tslint:disable-next-line: variable-name
  time_threshold?: number;
  videos?: Array<Video>;
  packages?: Array<Package>;
}
