import { User } from './user';

export class Record {
    recordable_type: string;
    recordable_id: string;
    user_id: string;
    log: string;
    created_at: string;

    user?: User;
}