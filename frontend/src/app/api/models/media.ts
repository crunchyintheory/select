import { UuidModel } from '../model';

// tslint:disable: variable-name
export class Media extends UuidModel {
    filename: string;
    uploader_id: string;
    url: string;
}
