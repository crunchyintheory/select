import { Injectable } from '@angular/core';
import { Model } from '../model';
import { ModelService } from '../model.service';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService extends ModelService<Subscription> {
  public route = 'subscriptions';
}

export class Subscription extends Model {
  name: string;
  price: number;
  credit: number;
  price_id?: string;
  description: string;
}
