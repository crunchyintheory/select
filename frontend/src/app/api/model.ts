export interface IModel {

  id?: number | string;
  // tslint:disable variable-name
  created_at?: Date;
  uploaded_at?: Date;
  // tslint:enable variable-name
}

export abstract class Model implements IModel {

  public id?: number;
  // tslint:disable variable-name
  public created_at?: Date;
  public uploaded_at?: Date;
  // tslint:enable variable-name

  public constructor(data?: { [key: string]: string }) {
    if (data) {
      Object.assign(this, data);
    }
  }
}

export abstract class UuidModel implements IModel {
  public id?: string;
  // tslint:disable variable-name
  public created_at?: Date;
  public uploaded_at?: Date;
  // tslint:enable variable-name

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public constructor(data?: { [key: string]: any }) {
    if (data) {
      Object.assign(this, data);
    }
  }
}
