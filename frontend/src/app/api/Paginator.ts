import { RequestBuilder } from './RequestBuilder';
import { Observable, PartialObserver, Subscription, Subject, OperatorFunction } from 'rxjs';
import { share } from 'rxjs/operators';
import { pipeFromArray } from 'rxjs/internal/util/pipe';
import { RequestBuilderOptions } from './RequestBuilderOptions';

/* eslint-disable @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-assignment */
export interface Paginator<T> extends RequestBuilder<T> {
    next(): this;
    take(limit?: number): this;
    skip(offset: number): this;
    start(observerOrNext?: PartialObserver<unknown> | ((value: T) => void),
          error?: (error: unknown) => void,
          complete?: () => void): this;
    complete(): void;
    // tslint:disable max-line-length
    pipe(): this;
    pipe<A>(op1: OperatorFunction<T, A>): Paginator<A>;
    pipe<A, B>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>): Paginator<B>;
    pipe<A, B, C>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>): Paginator<C>;
    pipe<A, B, C, D>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>): Paginator<D>;
    pipe<A, B, C, D, E>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>): Paginator<E>;
    pipe<A, B, C, D, E, F>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>): Paginator<F>;
    pipe<A, B, C, D, E, F, G>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>): Paginator<G>;
    pipe<A, B, C, D, E, F, G, H>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>): Paginator<H>;
    pipe<A, B, C, D, E, F, G, H, I>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>, op9: OperatorFunction<H, I>): Paginator<I>;
    pipe<A, B, C, D, E, F, G, H, I>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>, op9: OperatorFunction<H, I>, ...operations: OperatorFunction<any, any>[]): Paginator<unknown>;
    // tslint:enable max-line-length
}

export class OpenPaginator<T> extends RequestBuilder<T> implements Paginator<T> {

    // tslint:disable variable-name
    protected _skip = 0;
    protected _take = 0;
    // tslint:enable variable-name

    protected bigSubject: Subject<T>;
    protected bigObservable: Observable<T>;

    protected get observable() {
        this.request.parameters.set('skip', this._skip.toString());
        this.request.parameters.set('take', this._take.toString());
        if (!this._observable) {
            return this.formRequest(this.request).pipe(share());
        }
        return this._observable;
    }
    protected set observable(value: Observable<T>) {
        this._observable = value;
    }

    protected subscription: Subscription;

    public constructor(observable: Observable<T> | ((data: RequestBuilderOptions) => Observable<T>) | RequestBuilder<T>) {
        super(observable);

        this.bigSubject = new Subject<T>();
        this.bigObservable = this.bigSubject.asObservable();
    }

    public next() {
        return this.take();
    }

    public take(limit?: number) {
        if (limit) {
            this._skip += this._take;
            this._take = limit;
        }
        return this;
    }

    public skip(offset: number) {
        this._skip += offset;
        return this;
    }

    public reset() {
        this._take = 0;
        this._skip = 0;
        return this;
    }

    public start(observerOrNext?: PartialObserver<unknown> | ((value: T) => void),
                 error?: (error: unknown) => void,
                 complete?: () => void) {
        if (observerOrNext) {
            this.subscribe(observerOrNext, error, complete);
        }
        this.observable.subscribe(x => this.bigSubject.next(x));
        this._skip += this._take;
        return ClosedPaginator.make(this, this._skip, this._take, this.bigSubject, this.bigObservable);
    }

    public subscribe(observerOrNext?: PartialObserver<unknown> | ((value: T) => void),
                     error?: (error: unknown) => void,
                     complete?: () => void): Subscription {
        if (typeof observerOrNext === 'function') {
            return this.bigObservable.subscribe(observerOrNext, error, complete);
        } else {
            return this.bigObservable.subscribe(observerOrNext);
        }
    }

    public paginate() {
        return this;
    }

    public complete(): void {
        this.bigSubject.complete();
    }

    public pipe(): this;
    public pipe<A>(op1: OperatorFunction<T, A>): OpenPaginator<A>;
    public pipe<A, B>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>): OpenPaginator<B>;
    public pipe<A, B, C>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>): OpenPaginator<C>;
    public pipe<A, B, C, D>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>): OpenPaginator<D>;
    public pipe<A, B, C, D, E>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>): OpenPaginator<E>;
    public pipe<A, B, C, D, E, F>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>): OpenPaginator<F>;
    public pipe<A, B, C, D, E, F, G>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>): OpenPaginator<G>;
    public pipe<A, B, C, D, E, F, G, H>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>): OpenPaginator<H>;
    public pipe<A, B, C, D, E, F, G, H, I>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>, op9: OperatorFunction<H, I>): OpenPaginator<I>;
    public pipe<A, B, C, D, E, F, G, H, I>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>, op9: OperatorFunction<H, I>, ...operations: OperatorFunction<any, any>[]): OpenPaginator<unknown>;

    public pipe(...operations: Array<OperatorFunction<any, any>>): this {
        if (operations.length === 0) {
            return this;
        }

        this.bigObservable = pipeFromArray(operations)(this.bigObservable);
        return this;
    }
}

class ClosedPaginator<T> extends RequestBuilder<T> implements Paginator<T> {

    // tslint:disable variable-name
    protected _skip = 0;
    protected _take = 0;
    // tslint:enable variable-name

    protected bigSubject: Subject<T>;
    protected bigObservable: Observable<T>;

    public static make<T>(base: Paginator<T>, skip: number, take: number, bigSubject: Subject<T>, bigObservable: Observable<T>) {
        const c = new ClosedPaginator(base);
        c._skip = skip;
        c._take = take;
        c.bigSubject = bigSubject;
        c.bigObservable = bigObservable;
        return c;
    }

    protected get observable() {
        if (!this._observable) {
            return this.formRequest(this.request).pipe(share());
        }
        return this._observable;
    }
    protected set observable(value: Observable<T>) {
        this._observable = value;
    }

    protected doSubscription(): void {
        this.request.parameters.set('take', this._take.toString());
        this.request.parameters.set('skip', this._skip.toString());
        this.observable.subscribe(x => this.bigSubject.next(x));
    }

    public next() {
        return this.take();
    }

    public take(limit?: number) {
        if (limit) {
            this._take = limit;
        }
        this.doSubscription();
        this._skip += this._take;
        return this;
    }

    public skip(offset: number) {
        if (offset > 0) {
            this._skip += offset;
        }
        return this;
    }

    public reset() {
        this._take = 0;
        this._skip = 0;
        this.doSubscription();
        return this;
    }

    public start() {
        return this;
    }

    public subscribe(observerOrNext?: PartialObserver<unknown> | ((value: T) => void),
                     error?: (error: unknown) => void,
                     complete?: () => void): Subscription {
        if (typeof observerOrNext === 'function') {
            return this.bigObservable.subscribe(observerOrNext, error, complete);
        } else {
            return this.bigObservable.subscribe(observerOrNext);
        }
    }

    public paginate() {
        return this;
    }

    public complete(): void {
        this.bigSubject.complete();
    }

    public pipe(): this;
    public pipe<A>(op1: OperatorFunction<T, A>): ClosedPaginator<A>;
    public pipe<A, B>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>): ClosedPaginator<B>;
    public pipe<A, B, C>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>): ClosedPaginator<C>;
    public pipe<A, B, C, D>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>): ClosedPaginator<D>;
    public pipe<A, B, C, D, E>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>): ClosedPaginator<E>;
    public pipe<A, B, C, D, E, F>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>): ClosedPaginator<F>;
    public pipe<A, B, C, D, E, F, G>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>): ClosedPaginator<G>;
    public pipe<A, B, C, D, E, F, G, H>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>): ClosedPaginator<H>;
    public pipe<A, B, C, D, E, F, G, H, I>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>, op9: OperatorFunction<H, I>): ClosedPaginator<I>;
    public pipe<A, B, C, D, E, F, G, H, I>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>, op9: OperatorFunction<H, I>, ...operations: OperatorFunction<any, any>[]): ClosedPaginator<unknown>;

    public pipe(...operations: Array<OperatorFunction<any, any>>): this {
        if (operations.length === 0) {
            return this;
        }

        this.bigObservable = pipeFromArray(operations)(this.bigObservable);
        return this;
    }

}
