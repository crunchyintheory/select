import { Injectable } from '@angular/core';
import { DataService } from '../services/data.service';
import { IModel } from './model';
import { RequestBuilder } from './RequestBuilder';
import { GetRequestBuilder } from './GetRequestBuilder';

@Injectable({
  providedIn: 'root'
})
export abstract class ModelService<T extends IModel> {

  public abstract readonly route: string;

  constructor(protected data: DataService) { }

  public get(id: string): RequestBuilder<T> {
    return this.data.get<T>(`/${this.route}/${id}`).credentials();
  }

  public store<Response = unknown>(body: T): RequestBuilder<Response> {
    return this.data.post<T, Response>(`/${this.route}`, body).credentials();
  }

  public update<Response = T>(id: string | number, body: T): RequestBuilder<Response> {
    return this.data.put<T, Response>(`/${this.route}/${id}`, body).credentials();
  }

  public list(): GetRequestBuilder<T[]> {
    return this.data.get<T[]>(`/${this.route}`).credentials();
  }

  public destroy<Response = unknown>(id: string | number): RequestBuilder<Response> {
    return this.data.delete<Response>(`/${this.route}/${id}`).credentials();
  }
}

export abstract class SearchableModelService<T extends IModel> extends ModelService<T> {
  public list(search = ''): GetRequestBuilder<T[]> {
    return this.data.get<T[]>(`/${this.route}`).with('search', search);
  }
}
