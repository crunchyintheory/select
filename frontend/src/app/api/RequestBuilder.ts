import { PartialObserver, OperatorFunction, Observable, Subscription, Subscribable } from 'rxjs';
import { pipeFromArray } from 'rxjs/internal/util/pipe';
import { RequestBuilderOptions } from './RequestBuilderOptions';
import { share } from 'rxjs/operators';

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface Pair<T, U> {
    first: T;
    second: U;
}

// tslint:disable no-any
export class RequestBuilder<T = unknown> implements Subscribable<T> {
    protected request = new RequestBuilderOptions();

    // tslint:disable-next-line variable-name
    protected _observable?: Observable<T>;
    protected get observable() {
        if (!this._observable) {
            this._observable = this.formRequest(this.request).pipe(share());
        }
        return this._observable;
    }
    protected set observable(value: Observable<T>) {
        this._observable = value;
    }

    protected formRequest?: (data: RequestBuilderOptions) => Observable<T>;

    public constructor(observable: Observable<T> | ((data: RequestBuilderOptions) => Observable<T>) | RequestBuilder<T>) {
        if (typeof observable === 'function') {
            this.formRequest = observable;
        } else if (observable instanceof RequestBuilder) {

            this.request = observable.request;
            this._observable = observable._observable;
            this.formRequest = observable.formRequest;

        } else {
            this.observable = observable.pipe(share());
        }
    }

    public subscribe(observerOrNext?: PartialObserver<any> | ((value: T) => void),
                     error?: (error: any) => void,
                     complete?: () => void): Subscription {
        if (typeof observerOrNext === 'function') {
            return this.observable.subscribe(observerOrNext, error, complete);
        } else {
            return this.observable.subscribe(observerOrNext);
        }
    }

    public pipe(): this;
    public pipe<A>(op1: OperatorFunction<T, A>): RequestBuilder<A>;
    public pipe<A, B>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>): RequestBuilder<B>;
    public pipe<A, B, C>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>): RequestBuilder<C>;
    public pipe<A, B, C, D>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>): RequestBuilder<D>;
    public pipe<A, B, C, D, E>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>): RequestBuilder<E>;
    public pipe<A, B, C, D, E, F>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>): RequestBuilder<F>;
    public pipe<A, B, C, D, E, F, G>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>): RequestBuilder<G>;
    public pipe<A, B, C, D, E, F, G, H>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>): RequestBuilder<H>;
    public pipe<A, B, C, D, E, F, G, H, I>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>, op9: OperatorFunction<H, I>): RequestBuilder<I>;
    public pipe<A, B, C, D, E, F, G, H, I>(op1: OperatorFunction<T, A>, op2: OperatorFunction<A, B>, op3: OperatorFunction<B, C>, op4: OperatorFunction<C, D>, op5: OperatorFunction<D, E>, op6: OperatorFunction<E, F>, op7: OperatorFunction<F, G>, op8: OperatorFunction<G, H>, op9: OperatorFunction<H, I>, ...operations: OperatorFunction<any, any>[]): RequestBuilder<unknown>;

    /* eslint-disable @typescript-eslint/no-unsafe-assignment */
    // Safe due to overloads above
    public pipe(...operations: Array<OperatorFunction<any, any>>): this {
        if (operations.length === 0) {
            return this;
        }

        this.observable = pipeFromArray(operations)(this.observable);
        return this;
    }
    /* eslint-enable @typescript-eslint/no-unsafe-assignment */

    public with(keyvalue?: string): this;
    public with(key: string, value: string): this;
    public with(list: Array<[string, string]> | Array<Pair<string, string>> | Map<string, string>): this;

    public with(key?: string | Array<[string, string]> | Array<Pair<string, string>> | Map<string, string>, value?: string) {
        let list: Array<[string, string]>;
        if (typeof key === 'string') {
            if (value === undefined) {
                [key, value] = key.split('=');
            }
            list = [[key, value]];
        } else if (Array.isArray(key)) {
            if (key.length === 0) {
                return this;
            } else if (typeof key[0] === 'string') {
                list = key as Array<[string, string]>;
            } else {
                list = (key as Array<Pair<string, string>>).map(x => {
                    return [x.first, x.second];
                });
            }
        } else {
            list = Array.from(key.entries());
        }
        list.forEach(([k, v]) => {
            this.request.parameters.set(k, v);
        });
        return this;
    }

    public header(key: string, value: string): this {
        this.request.headers = this.request.headers.set(key, value);
        return this;
    }

    public credentials(): this {
        this.request.withCredentials = true;
        return this;
    }

    public noProgress(): this {
        this.request.headers = this.request.headers.set('ignoreProgressBar', '');
        return this;
    }

    public toPromise(): Promise<T> {
        return this.observable.toPromise();
    }
}
