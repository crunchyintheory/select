import { Component, OnInit, SecurityContext } from "@angular/core";
import { SubscriptionService, Subscription } from 'src/app/api/models/subscription';
import { StripeService } from 'src/app/services/stripe.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import marked from 'marked';
import { SettingService } from 'src/app/api/models/setting';

@Component({
    selector: 'app-subscribe',
    templateUrl: './subscribe.component.html',
    styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

    public subscriptions = new Array<Subscription>();

    public copy: SafeHtml;

    constructor(private service: SubscriptionService, private stripe: StripeService, public settings: SettingService, protected sanitizer: DomSanitizer) {
        this.settings.settings.subscribe(x => {
            if(Object.values(x).length) {
                this.copy = this.sanitizer.sanitize(SecurityContext.HTML, marked(x.subscribe_page_text));
            }
        });
    }

    ngOnInit() {
        this.service.list().subscribe(x => {
            this.subscriptions = x;
        });
    }

    click(subscription: Subscription) {
        this.stripe.subscribe(subscription.id);
    }
    
}