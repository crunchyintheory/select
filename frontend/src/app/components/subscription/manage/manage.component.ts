import { Component, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';
import { faPauseCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { SubscriptionService, Subscription as S } from 'src/app/api/models/subscription';

interface Subscription extends S {
    expanded?: boolean;
}

@Component({
    selector: 'app-manage-subscriptions',
    templateUrl: './manage.component.html',
    styleUrls: ['./manage.component.scss']
})
export class ManageSubscriptionsComponent implements OnInit {
    public faPauseCircle = faPauseCircle;
    public faCheckCircle = faCheckCircle;
    public subscriptions = new Array<Subscription>();
    public editSubscription: Subscription = { price: 0, name: '', credit: 0, description: '' };
    public creating = false;

    public constructor(private data: DataService, private service: SubscriptionService) {}

    public ngOnInit() {
        
        this.service.list().subscribe((x: Subscription[]) => {
            this.subscriptions = x.map(x => { x.expanded = false; return x; });
        });
    }

    public expand(subscription: Subscription, f = false) {
        if(!f && this.editSubscription && this.editSubscription.id === subscription.id) {
            this.editSubscription = { price: 0, name: '', credit: 0, description: '' };
            subscription.expanded = false; return;
        }
        this.subscriptions.forEach(x => x.expanded = false);
        this.editSubscription = JSON.parse(JSON.stringify(subscription)) as Subscription;
        this.editSubscription.price = (this.editSubscription.price / 100);
        this.editSubscription.credit = (this.editSubscription.credit / 100);
        subscription.expanded = true;
    }

    public addNew() {
        if(this.creating) {
            this.expand(this.subscriptions[0]);
        } else {
            const newSub = {
                name: '',
                price: 0,
                credit: 0,
                expanded: true
            } as Subscription;
            this.subscriptions = [newSub].concat(this.subscriptions);
            this.expand(newSub, true);
            this.creating = true;
        }
    }

    public save(subscription: Subscription) {
        if(subscription.id !== this.editSubscription.id) {
            alert('An Error occured');
        }
        Object.assign(subscription, this.editSubscription);
        if(subscription.id) {
            this.service.update(subscription.id, this.editSubscription).subscribe();
        }
        else {
            this.creating = false;
            this.service.store(this.editSubscription).subscribe((x: Subscription) => subscription.id = x.id);
        }
        this.editSubscription = null;
        this.subscriptions.forEach(x => x.expanded = false);
        subscription.price = subscription.price * 100;
        subscription.credit = subscription.credit * 100;
    }

    public delete(subscription: Subscription) {
        this.service.destroy(subscription.id).subscribe(() => this.subscriptions.splice(this.subscriptions.indexOf(subscription), 1));
    }
}