import { Input, Component, OnChanges } from '@angular/core';
import { Tag } from 'src/app/api/models/tag';

@Component({
    selector: 'app-tag',
    templateUrl: './tag.component.html',
    styleUrls: ['./tag.component.scss']
})
export class TagComponent implements OnChanges {
    @Input() public tag: Tag;
    @Input() public type: 'video' | 'package';

    public route: string;

    ngOnChanges() {
        switch(this.type) {
            case 'video': this.route = '/videos'; break;
            case 'package': this.route = '/packages'; break;
        }
    }
}
