import { Component, SecurityContext } from "@angular/core";
import { SettingService } from 'src/app/api/models/setting';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import marked from 'marked';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
    public now: number;

    public left: SafeHtml;
    public right: SafeHtml;

    public constructor(public settings: SettingService, private sanitizer: DomSanitizer) {
        this.now = new Date().getFullYear();

        this.settings.settings.subscribe(x => {
            if (Object.values(x).length) {
                this.left = this.sanitizer.sanitize(SecurityContext.HTML, marked(x.footer_left_text));
                this.right = this.sanitizer.sanitize(SecurityContext.HTML, marked(x.footer_right_text));
            }
        });
    }
}