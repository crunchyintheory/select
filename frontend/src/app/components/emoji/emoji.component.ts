import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-emoji',
  template: '<img *ngFor="let emoji of emojis" [src]="emoji[1]" [alt]="emoji[0]" [height]="height" />'
})
export class EmojiComponent implements OnChanges {

  @Input() public content: string;

  @Input() public height = 20;

  public emojis = new Array<[string, string]>();

  constructor() { }

  ngOnChanges(): void {
    const e = new Array<[string, string]>();
    for (const char of this.content) {
      e.push([char, `https://twemoji.maxcdn.com/v/latest/svg/${char.codePointAt(0).toString(16)}.svg`]);
    }

    this.emojis = e;
  }

}
