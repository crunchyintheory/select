import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TagService, Tag } from 'src/app/api/models/tag';
import { Package, PackageService } from 'src/app/api/models/package';
import { VideoService, Video } from 'src/app/api/models/video';
import { Paginator } from 'src/app/api/Paginator';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { SettingService } from 'src/app/api/models/setting';

@Component({
    selector: 'app-package-edit-component',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditPackageComponent implements OnInit {

    public create = null;

    public package: Package;
    public token: string;

    public form: FormGroup;

    public tags = [];
    public pReady = false;
    public tReady = false;

    public tagLoading = false;

    public videoPager: Paginator<Video[]>;
    public videos = new Array<Video>();

    public showMore = false;

    public exhausted = false;
    public startFresh = false;
    public onSearch = new Subject<string>();

    public get ready() {
        return this.pReady && this.tReady;
    }

    // tslint:disable-next-line: max-line-length
    constructor(public service: PackageService, public route: ActivatedRoute, public fb: FormBuilder, public tagService: TagService, protected router: Router, protected videoService: VideoService, public settings: SettingService) {
        this.form = this.fb.group({
            name: ['', Validators.required],
            description: [''],
            tags: [[]],
            bgcolor: ['#ffffff', Validators.pattern(/#[a-fA-F0-9]{6}/)],
            search: ['']
        });
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(paramMap => {
            const id = paramMap.get('id');
            this.create = !id;
            if (this.create) {
                this.package = new Package({ videos: [] });
                this.pReady = true;
            } else {
                this.service.get(id).subscribe(x => {
                    this.package = new Package(x);
                    const { name, description, tags, bgcolor } = x;
                    // tslint:disable-next-line: max-line-length
                    this.form.setValue({ name, description, tags: tags.filter(y => !y.time_threshold), bgcolor, search: '' });
                    this.pReady = true;
                });
            }

            this.tagService.list().with('duration', 'false').with('type', 'packages').subscribe(tags => {
                this.tags = tags;
                this.tReady = true;
            });

            this.videoPager = this.videoService.list().paginate(20).start(x => this.addMoreVideos(x));
            this.onSearch.pipe(
                debounceTime(1000)
            ).subscribe(x => {
                this.videos = [];
                this.videoPager.complete();
                this.startFresh = true;
                this.videoPager = this.videoService.list(x).paginate(20).start(y => this.addMoreVideos(y));
            });
        });
    }

    public addMoreVideos(videos: Video[]) {
        if (this.startFresh) {
            this.startFresh = false;
            this.videos = [];
        }

        videos = videos.filter(x => !this.package.videos.some(y => y.id === x.id));

        this.exhausted = !videos.length;

        this.videos = this.videos.concat(videos);
    }

    public addTagPromise(name: string) {
        return this.tagService.store<Tag>({ name }).toPromise();
    }

    public onSubmit() {
        const data: Package = {
            name: this.form.get('name').value as string,
            description: this.form.get('description').value as string,
            tags: this.form.get('tags').value as Tag[],
            bgcolor: this.form.get('bgcolor').value as string,
            videos: this.package.videos
        };
        if (this.create) {
            this.service.store(data).subscribe(() => {
                this.router.navigate(['/dashboard/packages']);
            });
        } else {
            this.service.update(this.package.id, data).subscribe(() => {
                this.router.navigate(['/dashboard/packages']);
            });
        }
    }

    public toggleShowMore() {
        this.showMore = !this.showMore;
    }

    public addVideo(video: Video, index: number) {
        this.videos.splice(index, 1);
        this.package.videos.push(video);
    }

    public search() {
        this.onSearch.next(this.form.get('search').value);
    }

    public delete() {
        this.service.destroy(this.package.id).subscribe(() => {
            this.router.navigate(['/dashboard/packages']);
        });
    }

}
