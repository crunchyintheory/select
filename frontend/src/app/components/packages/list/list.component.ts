import { Component, OnInit, OnDestroy } from '@angular/core';
import { Paginator } from 'src/app/api/Paginator';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { PackageService, Package } from 'src/app/api/models/package';
import { Tag, TagService } from 'src/app/api/models/tag';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-package-list-component',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListPackagesComponent implements OnInit, OnDestroy {

  public pager: Paginator<Package[]>;

  public packages = new Array<Package>();

  public searchInput = '';

  public onSearch = new Subject<string>();

  public isEnd = false;

  public startFresh = false;
  
  public tags = new Array<Tag>();

  public selectedTags = new Array<number>();

  constructor(public service: PackageService, public tagService: TagService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(x => {
      this.packages = [];
      this.pager = this.service.list().paginate(20);
      if(x['tag']) {
        this.selectedTags = [parseInt(x['tag'], 10)];
        this.pager.with('tags', x['tag']);
      }
      this.pager = this.pager.start(x => this.addMore(x));
      this.onSearch.pipe(
        debounceTime(1000)
      ).subscribe(x => this.doSearch(x));
  
      this.tagService.list().with('duration', 'false').with('type', 'packages').subscribe(x => this.tags = x);
    });
  }

  protected doSearch(search: string) {
    this.packages = [];
    this.pager.complete();
    this.startFresh = true;
    this.pager = this.service.list(search).with('tags', this.selectedTags.join(','))
      .paginate(20).start(y => this.addMore(y));
  }

  public onTagSelect() {
    this.doSearch(this.searchInput);
  }

  public nextPage(): void {
    this.pager.take();
  }

  ngOnDestroy() {
    this.pager.complete();
  }

  public addMore(packages: Package[]) {
    if (this.startFresh) {
      this.startFresh = false;
      this.packages = [];
    }

    this.isEnd = packages.length < 20;

    this.packages = this.packages.concat(packages);
  }

  public search() {
    this.onSearch.next(this.searchInput);
  }

  public loadMore() {
    this.pager.next();
  }

}
