import { Component, OnInit } from '@angular/core';
import { Package, PackageService } from 'src/app/api/models/package';
import { Paginator } from 'src/app/api/Paginator';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-packages-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManagePackagesComponent implements OnInit {

  public pager: Paginator<Package[]>;

  public packages = new Array<Package>();

  public searchInput = '';

  public onSearch = new Subject<string>();

  public isEnd = false;

  public startFresh = false;

  public deleted = false;


  constructor(public service: PackageService) { }

  ngOnInit(): void {
    this.pager = this.service.list().paginate(20).start(x => this.addMore(x));
    this.onSearch.pipe(
      debounceTime(1000)
    ).subscribe(x => {
      this.packages = [];
      this.pager.complete();
      this.startFresh = true;
      this.pager = this.service.list(x).paginate(20).with('deleted', this.deleted.toString()).start(y => this.addMore(y));
    });
  }

  public addMore(packages: Package[]) {
    if (this.startFresh) {
      this.startFresh = false;
      this.packages = [];
    }

    this.isEnd = !packages.length;

    this.packages = this.packages.concat(packages);
  }

  public search() {
    this.onSearch.next(this.searchInput);
  }

  public loadMore() {
    this.pager.next();
  }

  public toggleDeleted() {
    this.deleted = !this.deleted;
    
    this.onSearch.next(this.searchInput);
  }

  public forceDelete(pkg: Package, i: number) {
    this.packages.splice(i, 1);
    this.service.forceDelete(pkg.id).subscribe(() => {
      this.startFresh = true;
      this.onSearch.next(this.searchInput);
    });
  }

  public restore(pkg: Package) {
    this.service.restore(pkg.id).subscribe(() => {
      pkg.deleted_at = null;
    });
  }

}
