import { Component, Input } from '@angular/core';
import { Package } from 'src/app/api/models/package';
import { UserService } from 'src/app/api/models/user';
import { ExpiringVideo } from 'src/app/api/models/video';

@Component({
  selector: 'app-package-bubble',
  templateUrl: './bubble.component.html',
  styleUrls: ['./bubble.component.scss']
})
export class PackageBubbleComponent {

  @Input() package: Package;
  @Input() vs = 'md';
  @Input() exp = false;

  public constructor(public user: UserService) {}

  public expiring() {
    return this.package.videos as ExpiringVideo[];
  }

}
