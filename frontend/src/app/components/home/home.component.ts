import { Component, OnInit, SecurityContext } from '@angular/core';
import { VideoService, Video } from 'src/app/api/models/video';
import { PackageService, Package } from 'src/app/api/models/package';
import { UserService } from 'src/app/api/models/user';
import { SettingService } from 'src/app/api/models/setting';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import marked from 'marked';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public featuredVideo: Video;

  public packages = new Array<Package>();

  public copy1: SafeHtml;
  public copy2: SafeHtml;
  public _ready = 0;
  public get ready() { return (this._ready & 0x0f0f) === 0x0f0f; }
  public morecopy = false;

  public newInLibrary = [];

  constructor(public videoService: VideoService, public packageService: PackageService, public user: UserService, public settings: SettingService, protected sanitizer: DomSanitizer) {
    this.settings.settings.subscribe(x => {
      if (Object.values(x).length) {
        const [copy1, copy2] = x.home_page_text.split('&more;', 2);
        this.copy1 = this.sanitizer.sanitize(SecurityContext.HTML, marked(copy1));
        if (copy2) {
          this.copy2 = this.sanitizer.sanitize(SecurityContext.HTML, marked(copy2));
        } else {
          this.morecopy = true;
        }
        this._ready |= 0x000f;
      }
    });
  }

  ngOnInit(): void {
    this.videoService.featured().subscribe(x => {
      this.featuredVideo = new Video(x);
      this._ready |= 0x00f0;
    });

    this.packageService.list().paginate(5).start(x => {
      this.packages = x;
      this._ready |= 0x0f00;
    });
    
    this.user.user.subscribe(x => {
      if (!x.id) { return; }

      this.user.newInLibrary().subscribe(y => {
        this.newInLibrary = y;
        this._ready |= 0xf000;
      });
    });

  }

}
