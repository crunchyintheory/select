import { Component, OnInit } from '@angular/core';
import { User, UserService } from 'src/app/api/models/user';

@Component({
  selector: 'app-user-manage-component',
  template: `<div *ngFor='let user of users'><a [routerLink]='[user.id, "edit"]' [title]='user.id'>{{ user.first_name }} {{ user.last_name }}</a>
              &nbsp;&mdash;&nbsp;{{ user.videos?.length }} videos&nbsp;&mdash;&nbsp;{{ user.email }}
            </div>
  `
})
export class ManageUsersComponent implements OnInit {

  public users: Array<User>;

  constructor(public service: UserService) { }

  ngOnInit(): void {
    console.log('foo');
    this.service.list().subscribe(x => this.users = x);
  }

}
