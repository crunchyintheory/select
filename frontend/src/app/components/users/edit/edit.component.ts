import { Component, OnInit } from '@angular/core';
import { UserService, User } from 'src/app/api/models/user';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-edit-component',
  template: `<a href="https://dashboard.stripe.com/customers/{{ user.stripe_id }}" target="_blank">View on Stripe</a><br/><pre [innerText]='html'></pre>`
})
export class EditUserComponent implements OnInit {

  public user: User;
  public html: string;

  constructor(public service: UserService, private sanitizer: DomSanitizer, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
        this.service.get(params.get('id')).subscribe(x => { this.user = x; this.html = JSON.stringify(x, null, 2); });
    });
  }

}
