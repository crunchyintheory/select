import { Component, OnInit } from '@angular/core';
import { Video, VideoService } from 'src/app/api/models/video';
import { Paginator } from 'src/app/api/Paginator';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { faStar } from '@fortawesome/free-regular-svg-icons';

interface RecordVideo extends Video { count: number }

@Component({
  selector: 'app-videos-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageVideosComponent implements OnInit {

  public farStar = faStar;

  public pager: Paginator<Video[]>;

  public videos = new Array<RecordVideo>();

  public searchInput = '';

  public onSearch = new Subject<string>();

  public isEnd = false;

  public startFresh = false;

  public deleted = false;

  public featured = new Video();

  constructor(public service: VideoService) { }

  ngOnInit(): void {
    this.pager = this.service.list().paginate(20).start(x => this.addMore(x));
    this.onSearch.pipe(
      debounceTime(1000)
    ).subscribe(x => {
      this.videos = [];
      this.pager.complete();
      this.startFresh = true;
      this.pager = this.service.list(x).paginate(20).with('deleted', this.deleted.toString()).start(y => this.addMore(y));
    });

    this.service.featured().subscribe(x => this.featured = x);
  }

  public addMore(videos: Video[]) {
    if (this.startFresh) {
      this.startFresh = false;
      this.videos = [];
    }

    this.isEnd = !videos.length;

    const newVideos = videos.map(video => {
      Object.assign(video, { count: video.records.filter(x => x.log === 'purchased' || x.log === 'repurchased').length });
      return video;
    }) as Array<RecordVideo>;

    this.videos = this.videos.concat(newVideos);
  }

  public status(str: string): string {
    return str[0].toUpperCase() + str.substr(1);
  }

  public statusIcon(str: string): string {
    switch (str) {
      case 'published':
        return 'eye';
      case 'draft':
        return 'eye-slash';
      default:
        return 'angry';
    }
  }

  public search() {
    this.onSearch.next(this.searchInput);
  }

  public loadMore() {
    this.pager.next();
  }

  public toggleDeleted() {
    this.deleted = !this.deleted;
    
    this.onSearch.next(this.searchInput);
  }

  public forceDelete(video: Video, i: number) {
    this.videos.splice(i, 1);
    this.service.forceDelete(video.id).subscribe(() => {
      this.startFresh = true;
      this.onSearch.next(this.searchInput);
    });
  }

  public restore(video: Video) {
    this.service.restore(video.id).subscribe(() => {
      video.deleted_at = null;
    });
  }

  public feature(video: Video) {
    this.featured = this.featured.id === video.id ? new Video() : video;
    this.service.setFeatured(video.id).subscribe(x => {
      this.featured = x ? x : new Video();
    });
  }

}
