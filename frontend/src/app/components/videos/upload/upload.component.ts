import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { VideoService, Video } from 'src/app/api/models/video';
import { Subscribable, BehaviorSubject } from 'rxjs';
import { map, finalize, shareReplay, pluck } from 'rxjs/operators';
import { UserService } from 'src/app/api/models/user';
import { ActivatedRoute } from '@angular/router';
import { TagService, Tag } from 'src/app/api/models/tag';

interface UploadData {
  name: string;
  fileName: string;
  description: string;
  price: string;
  tags: Array<Tag>;
  visibility: 'published' | 'draft';
  upload: Subscribable<string>;
  error: Subscribable<string>;
  expanded: boolean;
  complete: Subscribable<string>;
  thumb: Subscribable<string>;
  status: Subscribable<'error' | 'progress' | 'complete'>;
}

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  @ViewChild('file') public file: ElementRef<HTMLInputElement>;

  public uploads = new Array<UploadData>();

  public finishedUploads = new Array<UploadData>();

  public uploadSettings: {
    url: string;
    headers: {
        [key: string]: string;
    }
  };

  public title = true;
  public tags = [];

  public get allUploads() {
    return this.uploads.filter(x => x !== null).concat(this.finishedUploads);
  }

  constructor(public videos: VideoService, public user: UserService, public tagService: TagService, public route: ActivatedRoute) {
    this.route.data.subscribe(x => {
      this.title = x.title !== undefined ? x.title as boolean : true;
    });
  }

  ngOnInit() {
    this.user.upload().subscribe(x => this.uploadSettings = x);
    /*const bs = new BehaviorSubject('50.00');
    const c = new BehaviorSubject(new Video({id: '1', name: 'foo', videouid: 'bar', thumbnail: { url: 'https://http.cat/100' }}));
    this.uploads.push({
      name: '',
      fileName: 'test1',
      description: '',
      visibility: 'published',
      upload: bs.pipe(finalize(() => this.onEnd(0)), shareReplay(1)),
      error: new Observable(),
      expanded: false,
      complete: c.pipe(pluck('id')),
      thumb: c.pipe(map(x => `url(${x.thumbnail.url})`)),
      status: new BehaviorSubject('progress'),
      tags: [],
      price: '0.00'
    });
    this.finishedUploads.push({
      name: '',
      fileName: 'test2',
      description: '',
      visibility: 'published',
      upload: new BehaviorSubject('100.00'),
      error: new Observable(),
      expanded: false,
      complete: new BehaviorSubject(new Video({id: '1', name: 'foo', videouid: 'bar'})).pipe(pluck('id')),
      thumb: (new Subject<Video>()).pipe(map(x => `url(${x.thumbnail.url})`)),
      status: new BehaviorSubject('complete'),
      tags: [],
      price: '0.00'
    });
    setTimeout(() => { bs.next('100.00'); bs.complete(); }, 5000);*/

    this.tagService.list().subscribe(tags => {
        this.tags = tags.filter(x => !x.time_threshold);
    });
  }

  public addTagPromise(name: string) {
      return this.tagService.store<Tag>({ name }).toPromise();
  }

  public start(files: FileList) {
    // tslint:disable-next-line: prefer-for-of (can't use with FileList)
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const index = this.uploads.length;
      const complete = new BehaviorSubject<Video>(new Video());
      const error = new BehaviorSubject<string>(null);
      const status = new BehaviorSubject<'error' | 'progress' | 'complete'>('progress');
      const upload = this.videos.upload(this.uploadSettings.url, this.uploadSettings.headers, file, (videouid: string) => {
        const { name, description, visibility, tags, price } = this.uploads[index];
        console.log(this.uploads[index]);
        return new Video({
          name,
          description,
          status: visibility,
          videouid,
          tags,
          price
        });
      }, complete, error, status);
      this.uploads.push({
        name: file.name.split('.')[0],
        fileName: file.name,
        description: '',
        visibility: 'published',
        upload: upload.pipe(map(x => x.toFixed(2)), finalize(() => this.onEnd(index)), shareReplay(1)),
        error,
        status,
        expanded: false,
        complete: complete.pipe(pluck('id')),
        thumb: complete.pipe(map(x => `url(${x.thumbnail?.url})`)),
        tags: [],
        price: '0.00'
      });
    }
    this.file.nativeElement.value = null;
  }

  private onEnd(index: number) {
    const upload = this.uploads.splice(index, 1, null)[0];
    upload.expanded = false;
    this.finishedUploads.push(upload);
  }

}
