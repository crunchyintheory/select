import { Component, OnInit } from '@angular/core';
import { VideoService, Video } from 'src/app/api/models/video';
import { ActivatedRoute } from '@angular/router';
import { SettingService } from 'src/app/api/models/setting';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.scss']
})
export class SingleVideoComponent implements OnInit {

  public video: Video;
  public relatedVideos: Array<Video> = [];

  constructor(public service: VideoService, public route: ActivatedRoute, public settings: SettingService, public title: TitleService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(paramMap => {
      const id = paramMap.get('id');
      this.service.get(id).subscribe(x => {
        this.video = new Video(x);
        this.title.setTitle(this.video.name);
      });

      this.service.list().paginate(4).start(x => this.relatedVideos = x);
    });
  }

}
