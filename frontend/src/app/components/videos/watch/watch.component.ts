import { Component, OnInit } from '@angular/core';
import { VideoService, Video } from 'src/app/api/models/video';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { SettingService } from 'src/app/api/models/setting';
import { TitleService } from 'src/app/services/title.service';
import { Package, PackageService } from 'src/app/api/models/package';
import { ReplaySubject, zip, Subscription, BehaviorSubject, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-watch',
  templateUrl: './watch.component.html',
  styleUrls: ['./watch.component.scss']
})
export class WatchVideoComponent implements OnInit {

  public video: Video;
  public videoReady = new Subject<boolean>();
  
  public package: Package;
  public packageReady = new BehaviorSubject(false);
  public currentIndex: number;

  private paramSub: Subscription;
  private querySub: Subscription;

  constructor(private service: VideoService, private route: ActivatedRoute, public settings: SettingService, private router: Router, private title: TitleService, private packages: PackageService) { }

  ngOnInit(): void {
    this.videoReady.subscribe(() => {
      this.packageReady.pipe(filter(x => x)).subscribe(() => {
        this.currentIndex = this.package.videos.findIndex(x => x.id === this.video.id) + 1;
      });
    });
    this.paramSub = this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      this.service.canWatch(id).subscribe(() => { }, () => {
        this.router.navigate(['videos', id]);
      });
      this.service.get(id).subscribe(x => {
        this.video = new Video(x);
        this.videoReady.next(true);
        this.title.setTitle(this.video.name);
      });
    });
    this.querySub = this.route.queryParamMap.subscribe(query => {
      if(query.has('package')) {
        this.packages.get(query.get('package')).subscribe(x => {
          this.package = x;
          this.packageReady.next(true);
        });
      }
    });
  }

  public ended(): void {
    if(this.package && this.currentIndex < this.package.videos.length) {
      this.router.navigate(['/videos', this.package.videos[this.currentIndex].id, 'watch'], { queryParams: { package: this.package.id }});
    }
  }

  ngOnDestroy() {
    this.paramSub.unsubscribe();
    this.querySub.unsubscribe();
  }

}
