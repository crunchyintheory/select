import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { VideoService, Video } from 'src/app/api/models/video';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TagService, Tag } from 'src/app/api/models/tag';

@Component({
    selector: 'app-video-edit-component',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditVideoComponent implements OnInit {

    public video: Video;
    public token: string;

    public form: FormGroup;

    public tags = [];
    public vReady = false;
    public tReady = false;
    public wReady = false;

    public tagLoading = false;

    public get ready() {
        return this.vReady && this.tReady && this.wReady;
    }

    @ViewChild('video') public stream: ElementRef<HTMLElement>;

    // tslint:disable-next-line: max-line-length
    constructor(public service: VideoService, public route: ActivatedRoute, public fb: FormBuilder, public tagService: TagService, protected router: Router) {
        this.form = this.fb.group({
            name: ['', Validators.required],
            description: [''],
            status: ['published', Validators.pattern(/published|draft/)],
            tags: [[]],
            price: ['', [Validators.required, Validators.pattern(/\d+(?:\.\d{1,2})?/)]]
        });
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(paramMap => {
            const id = paramMap.get('id');
            this.service.get(id).subscribe(x => {
                this.video = new Video(x);
                const { name, description, status, tags, price } = x;
                // tslint:disable-next-line: max-line-length
                this.form.setValue({ name, description, status, tags: tags.filter(y => !y.time_threshold), price: (price / 100).toFixed(2) });
                this.vReady = true;
            });

            this.tagService.list().with('duration', 'false').with('type', 'videos').subscribe(tags => {
                this.tags = tags;
                this.tReady = true;
            });

            this.service.watch(id).subscribe(x => {
                this.token = x.token;
                this.wReady = true;
            });
        });
    }

    public addTagPromise(name: string) {
        return this.tagService.store<Tag>({ name }).toPromise();
    }

    public onSubmit() {
        /* eslint-disable @typescript-eslint/no-unsafe-assignment */
        const data: Video = {
            name: this.form.get('name').value,
            description: this.form.get('description').value,
            status: this.form.get('status').value,
            tags: this.form.get('tags').value,
            price: this.form.get('price').value
        };
        this.service.update(this.video.id, data).subscribe(() => {
            this.router.navigate(['/dashboard/videos']);
        });
    }

    public delete() {
        this.service.destroy(this.video.id).subscribe(() => {
            this.router.navigate(['/dashboard/videos']);
        });
    }

}
