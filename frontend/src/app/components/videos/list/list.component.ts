import { Component, OnInit, OnDestroy } from '@angular/core';
import { VideoService, Video } from 'src/app/api/models/video';
import { Paginator } from 'src/app/api/Paginator';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { TagService, Tag } from 'src/app/api/models/tag';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-video-list-component',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListVideosComponent implements OnInit, OnDestroy {

  public pager: Paginator<Video[]>;

  public videos = new Array<Video>();

  public searchInput = '';

  public onSearch = new Subject<string>();

  public isEnd = false;

  public startFresh = false;
  
  public tags = new Array<Tag>();

  public selectedTags = new Array<number>();

  constructor(public service: VideoService, public tagService: TagService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(x => {
      this.videos = [];
      this.pager = this.service.list().paginate(20);
      if(x['tag']) {
        this.selectedTags = [parseInt(x['tag'], 10)];
        this.pager.with('tags', x['tag']);
      }
      this.pager = this.pager.start(x => this.addMore(x));
      this.onSearch.pipe(
        debounceTime(1000)
      ).subscribe(x => this.doSearch(x));
  
      this.tagService.list().with('type', 'videos').subscribe(x => this.tags = x);
    });
  }

  protected doSearch(search: string) {
    this.videos = [];
    this.pager.complete();
    this.startFresh = true;
    this.pager = this.service.list(search).with('tags', this.selectedTags.join(','))
      .paginate(20).start(y => this.addMore(y));
  }

  public onTagSelect() {
    this.doSearch(this.searchInput);
  }

  public nextPage(): void {
    this.pager.take();
  }

  ngOnDestroy() {
    this.pager.complete();
  }

  public addMore(videos: Video[]) {
    if (this.startFresh) {
      this.startFresh = false;
      this.videos = [];
    }

    this.isEnd = videos.length < 20;

    this.videos = this.videos.concat(videos);
  }

  public search() {
    this.onSearch.next(this.searchInput);
  }

  public loadMore() {
    this.pager.next();
  }

}
