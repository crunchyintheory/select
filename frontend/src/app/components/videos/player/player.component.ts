import { Component, ViewChild, ElementRef, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { VideoService, Video } from 'src/app/api/models/video';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnChanges {

  @Input() public video: Video;
  
  @Output() ended = new EventEmitter<CustomEvent>();

  public token: string;
  public thumbnail: string;
  public loading = false;

  @ViewChild('video') public stream: ElementRef<HTMLElement>;

  constructor(public service: VideoService) { }

  ngOnChanges(): void {
      this.token = null;
      if (this.video?.thumbnail?.url) {
        this.thumbnail = this.video.thumbnail.url.replace('_sd', '_hd');
      }
  }

  watch() {
    if (!this.loading || this.token) {
      this.loading = true;
      this.service.watch(this.video.id).subscribe(x => {
        this.token = x.token;
      });
    }
  }

  loaded() {
    this.loading = false;
  }

}
