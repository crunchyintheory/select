import { Component, OnInit } from '@angular/core';
import { ExpiringVideo } from 'src/app/api/models/video';
import { UserService } from 'src/app/api/models/user';
import { Package } from 'src/app/api/models/package';
import { SettingService } from 'src/app/api/models/setting';

@Component({
  selector: 'app-video-library-component',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit{

  public videos = new Array<ExpiringVideo>();
  public packages = new Array<Package>();

  constructor(public service: UserService, public settings: SettingService) { }

  ngOnInit(): void {
    this.service.getLibrary().subscribe(x => { this.videos = x.videos; this.packages = x.packages; });
  }

}
