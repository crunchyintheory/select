import { Component, Input, HostBinding, OnChanges } from '@angular/core';
import { Video } from 'src/app/api/models/video';
import { UserService } from 'src/app/api/models/user';
import { Params } from '@angular/router';

@Component({
  selector: 'app-video-bubble',
  templateUrl: './bubble.component.html',
  styleUrls: ['./bubble.component.scss']
})
export class VideoBubbleComponent implements OnChanges {

  @Input() video: Video;
  @Input() title = true;
  @Input() duration = true;
  @HostBinding('class.description') @Input() description = false;
  @HostBinding('class') @Input() size: 'sm' | 'md' | 'lg' = 'md';
  @Input() noblur = false;
  @HostBinding('class.noblur') noblurClass = false;
  @HostBinding('class.expired') expired = false;
  @Input() cfstate = false;
  @Input() edit = false;
  @Input() editButton = false;
  @Input() link = true;
  @Input() exp: string = null;
  @Input() watch: string | boolean = false;

  public expirationHours = null;
  public expirationMinutes = null;

  public cficon = 'sync-alt';
  public desc = '';

  public constructor(public user: UserService) {}

  public get route() {
    if (!this.link) {
      return null;
    } else if (this.edit) {
      return ['/dashboard/videos', this.video.id, 'edit'];
    } else if (this.watch) {
      if(typeof this.watch === 'string') {
        this.parameters = { package: this.watch};
      }
      return ['/videos', this.video.id, 'watch'];
    } else {
      return ['/videos', this.video.id];
    }
  }

  public parameters: Params = {};

  ngOnChanges() {
    if (!this.noblur) {
      this.noblurClass = !this.title;
    }
    switch (this.video.cfstate) {
      case 'queued':
      case 'inprogress':
      case 'unuploaded':
        this.cficon = 'sync-alt';
        break;
      case 'ready':
        this.cficon = 'check-circle';
        break;
      default:
        this.cficon = 'times-circle';
        break;
    }
    if (this.video.description && this.video.description.length > 100) {
      this.desc = this.video.description.substr(0, 97) + '...';
    } else {
      this.desc = this.video.description;
    }

    if (this.exp) {
      const expiration = new Date(this.exp);
      if (expiration <= new Date()) {
        this.expired = true;
      }

      const diff = expiration.getTime() - (new Date()).getTime();

      this.expirationHours = Math.floor(diff / 1000 / 60 / 60);
      this.expirationMinutes = Math.floor(diff / 1000 / 60 - this.expirationHours * 60);
    }
  }

  public prettyStatus() {
    switch (this.video.cfstate) {
      case 'queued':
      case 'inprogress':
        return 'Processing (Only you can see this video)';
      case 'ready':
        return 'Ready';
      case 'unuploaded':
        return 'Un-uploaded';
      default:
        return 'Error';
    }
  }

}
