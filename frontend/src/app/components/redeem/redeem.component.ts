import { Component } from "@angular/core";
import { UserService } from 'src/app/api/models/user';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
    'selector': 'app-redeem',
    'templateUrl': './redeem.component.html'
})
export class RedeemComponent {

    public valid = false;
    public code = '';

    public constructor(public user: UserService, public router: Router) {}

    public redeem() {
        this.user.redeem(this.code).subscribe(x => {
            if(x) {
                Swal.fire({
                    title: 'Success',
                    text: 'Gift Card redeemed successfully',
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 3000,
                    icon: 'success'
                });
                this.user.current(true);
                this.router.navigate(['/home']);
            } else {
                Swal.fire({
                    title: 'Error',
                    text: 'Gift Card could not be redeemed',
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 3000,
                    icon: 'error'
                });
            }
        });
    }

}