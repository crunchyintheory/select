import { Component, ViewChild, ElementRef, AfterViewInit, Output, Input, OnChanges } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import sha256 from 'crypto-js/sha256';
import CryptoJS from 'crypto-js';

@Component({
    selector: 'app-redeem-input',
    template: `
    <div class="input-group">
        <input class="form-control" type="text" #fragment0 name="fragment0" [(ngModel)]='fragments[0]' (keyup)='onKeyUp(0, $event)' [readonly]='value'/>
        <div class="input-group-text input-group-prepend input-group-append">
            -
        </div>
        <input class="form-control" type="text" #fragment1 name="fragment1" [(ngModel)]='fragments[1]' (keyup)='onKeyUp(1, $event)' maxlength="4" [readonly]='value'/>
        <div class="input-group-text input-group-prepend input-group-append">
            -
        </div>
        <input class="form-control" type="text" #fragment2 name="fragment2" [(ngModel)]='fragments[2]' (keyup)='onKeyUp(2, $event)' maxlength="4" [readonly]='value'/>
        <div class="input-group-text input-group-prepend input-group-append">
            -
        </div>
        <input class="form-control" type="text" #fragment3 name="fragment3" [(ngModel)]='fragments[3]' (keyup)='onKeyUp(3, $event)' maxlength="4" [readonly]='value'/>
    </div>
    <div class="mt-3 text-danger text-center" [ngStyle]='{ opacity: invalid ? 1 : 0 }'>The provided code is invalid.</div>
    `,
    styles: [`
    .input-group {
        width: 375px;
        font-family: 'Courier New', monospace;
    }
    .input-group-text {
        border-radius: 0 !important;
    }
    `]
})
export class RedeemInputComponent implements AfterViewInit, OnChanges {

    @ViewChild('fragment0') public fragment0: ElementRef<HTMLInputElement>;
    @ViewChild('fragment1') public fragment1: ElementRef<HTMLInputElement>;
    @ViewChild('fragment2') public fragment2: ElementRef<HTMLInputElement>;
    @ViewChild('fragment3') public fragment3: ElementRef<HTMLInputElement>;

    public fragments = [
        '',
        '',
        '',
        ''
    ];

    public inputs: Array<ElementRef<HTMLInputElement>>;

    public invalid = false;

    @Output() public input = new BehaviorSubject<string>('');
    @Output() public valid = new BehaviorSubject<boolean>(false);
    @Input() public value = '';

    ngOnChanges() {
        this.value.split('-').map((x, i) => this.fragments[i] = x);
    }

    ngAfterViewInit() {
        this.inputs = [
            this.fragment0,
            this.fragment1,
            this.fragment2,
            this.fragment3
        ];
    }

    onKeyUp(num: number, evt: KeyboardEvent) {
        let skipNav = false;
        if(num === 0) {
            const input = this.fragments[0].substr(0, 19).split('-').filter(x => x.length === 4);
            if (input.length === 4) {
                input.map((x, i) => { this.fragments[i] = x; this.inputs[i].nativeElement.value = x; });
                this.inputs[3].nativeElement.focus();
                skipNav = true;
            }
        }
        this.fragments[num] = this.inputs[num].nativeElement.value.substr(0, 4);
        this.inputs[num].nativeElement.value = this.fragments[num];
        this.validate();
        if (!skipNav) {
            if (num > 0 && evt.key === 'Backspace' && this.fragments[num] === '') {
                this.inputs[num - 1].nativeElement.focus();
            }
            else if (num < 3 && this.fragments[num].length >= 4) {
                this.inputs[num + 1].nativeElement.focus();
            }
        }
    }

    private validate() {
        if (this.fragments[0].length === 4) {
            const code = this.fragments.join('');
            let correct = '';
            const seed = Number.parseInt(sha256(code.toLowerCase()).toString(CryptoJS.enc.Hex).substr(0, 4), 16);
            let bit = 1;
            for (let i = 0; i < code.length; i++) {
                correct += (bit & seed) ? code[i].toUpperCase() : code[i].toLowerCase();
                bit = bit << 1;
            }
            this.invalid = code !== correct;
        } else {
            this.invalid = false;
        }
        this.valid.next(!this.invalid && this.fragments.every(x => x.length === 4));
        this.input.next(this.fragments.join('-'));
    }
    
}