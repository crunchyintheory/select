import { Component, SecurityContext } from '@angular/core';
import { UserService, User } from 'src/app/api/models/user';
import { SettingService } from 'src/app/api/models/setting';
import { BehaviorSubject } from 'rxjs';
import { debounceTime, take } from 'rxjs/operators';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import marked from 'marked';
import Swal from 'sweetalert2';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  public ready = false;

  public _collapse = new BehaviorSubject(true);
  public collapse = this._collapse.asObservable().pipe(debounceTime(5));

  private _menu: { [key: string]: { link: string, display: string, loggedin?: boolean, hideIfSubscribed?: boolean } } = {
      videos: { link: '/videos', display: 'Videos', },
      packages: { link: '/packages', display: 'Packages' },
      library: { link: '/videos/library', display: 'My Library', loggedin: true },
      subscribe: { link: '/subscribe', display: 'Subscribe', loggedin: true, hideIfSubscribed: true }
  };

  public get menu() {
      return Object.values(this._menu);
  }

  public user: User;

  public dropdown = false;

  public resent = false;

  public resending = false;

  public subscribeSwalCopy: SafeHtml;

  constructor(public service: UserService, public settings: SettingService, protected sanitizer: DomSanitizer) {
    this.service.user.subscribe(x => this.user = x);

    this.settings.settings.subscribe(x => {
      this._menu.videos.display = x.video_name_plural_listing;
      this._menu.packages.display = x.package_name_plural_listing;
      if (Object.values(x).length) {
        this.ready = true;
        this.subscribeSwalCopy = this.sanitizer.sanitize(SecurityContext.HTML, marked(x.manage_subscription_dialog_text));
      }
    });
  }

  public onBlur() {
    setTimeout(() => this.dropdown = false, 100);
  }

  public logout() {
    this.service.logout();
  }

  public toggleCollapse() {
    this.collapse.pipe(take(1)).subscribe(x => {
      this._collapse.next(!x);
    });
  }

  public resend() {
    if(this.resending) {
      return;
    }
    this.resending = true;
    this.service.resendVerification().subscribe(() => {
      this.resent = true;
    });
  }

  public portal() {
    Swal.fire({
        title: `Leaving ${this.settings.settings?.value?.site_name ?? 'Select'}`,
        icon: 'info',
        iconHtml: `<svg viewBox="0 0 512 512" style="padding: 1rem"><path fill="currentColor" d="${faSignOutAlt.icon[4] as string}"></path></svg>`,
        html: this.subscribeSwalCopy,
        showCancelButton: true,
        cancelButtonText: 'Close',
        confirmButtonText: 'Continue',
        showLoaderOnConfirm: true,
        preConfirm: () => {
          this.service.createPortalSession().subscribe(x => {
            window.location.href = x;
          });
        }
    });
  }

}
