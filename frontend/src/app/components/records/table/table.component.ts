import { Component, Input } from "@angular/core";
import { Record } from 'src/app/api/models/record';

@Component({
    selector: 'app-records-table',
    template: `
        <table class="table">
            <thead>
                <tr>
                    <th>User</th>
                    <th>Event</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <tr *ngFor='let record of records'>
                    <td><a [routerLink]='["/dashboard", "users", record.user.id, "edit"]'>{{ record.user.first_name }} {{ record.user.last_name }}</a></td>
                    <td><span class="badge badge-pill {{ record.log }}"><fa-icon [icon]='icon(record)'></fa-icon>&nbsp;{{ record.log }}</span></td>
                    <td>{{ record.created_at | date:'long' }}</td>
                </tr>
            </tbody>
        </table>
    `,
    styleUrls: ['./table.component.scss']
})
export class RecordsTableComponent {

    @Input() public records: Array<Record>;

    public icon(record: Record) {
        switch(record.log) {
            case 'purchased':
            case 'repurchased':
                return 'shopping-bag';
            case 'started_watching':
                return 'eye';
        }
    }

}