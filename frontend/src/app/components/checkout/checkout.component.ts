import { Component, OnInit } from '@angular/core';
import { StripeService } from 'src/app/services/stripe.service';
import { UserService, User } from 'src/app/api/models/user';
import { Video } from 'src/app/api/models/video';
import { skipUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  public cart: Array<Video> = [];
  public currentUser: User = null;
  public promoError: string;

  constructor(public stripe: StripeService, public user: UserService) { }

  ngOnInit(): void {
    const hasCart = new Subject();
    this.user.cart.pipe(skipUntil(hasCart)).subscribe(x => this.cart = x);
    this.user.current(true).subscribe(x => { this.currentUser = x; this.cart = x.cart; hasCart.next(); });
  }

  public checkout() {
    this.stripe.checkout();
  }

  public total(withPromo = true) {
    return (this.cart.reduce((acc, cv) => acc + cv.price, 0) / 100) * (withPromo ? 1 - (this.currentUser?.promo?.discount || 0) / 10000 : 1);
  }

  public remove(video: Video) {
    this.user.removeFromCart(video.id).subscribe();
  }

  public applyCode(code: string) {
    this.stripe.applyPromo(code).subscribe(x => {
      this.promoError = null;
      this.user.setPromo(x);
    }, () => {
      this.promoError = 'Invalid promo code.';
    });
  }

  public removeCode() {
    this.stripe.removePromo().subscribe(() => {
      this.user.setPromo(null);
    });
  }

}
