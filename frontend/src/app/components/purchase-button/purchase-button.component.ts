import { Component, Input, OnChanges, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { UserService } from 'src/app/api/models/user';
import { Video } from 'src/app/api/models/video';
import { Package } from 'src/app/api/models/package';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-purchase-button',
    template: `
        <div class="buy-button d-flex align-items-center justify-content-end" [ngClass]='this.state' *ngIf='service.signedIn && (service.user | async).email_verified_at && amount !== null && this.state !== null'>
            <div class="border border-right-0 amount" style="color: #000"><ng-container *ngIf='state === "add"'>{{ amount }}</ng-container></div>
            <button (click)='onClick()' type="button" class="btn btn-primary">
                <ng-container *ngIf='state === "add"'>Add to Cart</ng-container>
                <div class="spinner-border" role="status" *ngIf='state === "adding"'>
                    <span class="sr-only">Loading...</span>
                </div>
                <ng-container *ngIf='state === "added"'><fa-icon icon="check"></fa-icon>&nbsp;Added to Cart</ng-container>
                <ng-container *ngIf='state === "library"'>Watch</ng-container>
            </button>
        </div>
        <div *ngIf='!service.signedIn' class="text-right">
            <p class="text-dark"><a routerLink='/login'>Login or Register</a> to purchase videos.</p>
        </div>
        <div *ngIf='service.signedIn && !(service.user | async).email_verified_at'>
            <p class="text-dark">Verify your email to purchase</p>
        </div>
        `,
    styleUrls: ['./purchase-button.component.scss']
})
export class PurchaseButtonComponent implements OnChanges {

    @Input() public cents: number = null;

    @Input() public credits: number = null;

    @Output() public addedToCart = new Subject<boolean>();

    @Input() public cartable: Video | Package = null;

    @Input() public type: 'video' | 'package' = 'video';

    public amount: string = null;

    protected id: string = null;

    public state: 'add' | 'adding' | 'added' | 'library' = null;

    constructor(public service: UserService, protected router: Router) { }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    private isVideo(obj: Video | Package | string): obj is Video {
        return this.type === 'video';
    }
    
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    private isPackage(obj: Video | Package | string): obj is Package {
        return this.type === 'package';
    }

    protected compute() {
        let inLibrary = false;
        if (typeof this.cartable === 'string') {
            this.id = this.cartable;
        } else {
            this.id = this.cartable.id;
        }
        if (this.cents) {
            this.amount = `$${(this.cents / 100).toFixed(2)}`;
        } else if (this.credits) {
            this.amount = `${this.credits} credits`;
        } else if (typeof this.cartable !== 'string') {
            this.amount = `$${(this.cartable.price / 100).toFixed(2)}`;
            if (this.cartable.price === 0) {
                this.state = 'library';
                return;
            }
        } else {
            this.state = 'library';
            return;
        }
        this.service.can('admin').subscribe(x => {
            inLibrary = x;
            if (x) {
                this.state = 'library';
            } else if (this.isVideo(this.cartable)) {
                this.service.inCart(this.id).subscribe(x => {
                    if (x) {
                        this.state = 'added';
                    } else if (!inLibrary) {
                        this.state = 'add';
                    }
                });
                this.service.inLibrary(this.id).subscribe(x => {
                    inLibrary = x;
                    if (x) {
                        this.state = 'library';
                    }
                });
            } else if(typeof this.cartable === 'string') {
                this.state = 'add';
            } else {
                this.service.packageInCart(this.cartable).subscribe(x => {
                    if(x) {
                        this.state = 'added';
                    } else if (!inLibrary) {
                        this.state = 'add';
                    }
                });
                this.service.packageInLibrary(this.cartable).subscribe(x => {
                    inLibrary = x;
                    if (x) {
                        this.state = 'library';
                    }
                });
            }
        });
    }

    ngOnChanges(): void {
        this.compute();
    }

    public onClick(): void {
        if (this.state === 'add') {
            this.state = 'adding';
            const cb = () => {
                this.service.addToCart(this.id, this.type).subscribe(() => {
                    this.state = 'added';
                });
            };
            if(this.service.user.value.has_seen_cart_warning) {
                cb();
            } else {
                Swal.fire({
                    title: 'Info',
                    icon: 'info',
                    text: 'Once you have made your purchase, you will have 72 hours to start watching your video. After you start watching one, you will have 24 hours to finish it. Videos purchased as packages have individual timers.',
                    confirmButtonText: 'Ok, don\'t show me this again.'
                }).then(result => {
                    if(result) {
                        const u = this.service.user.value;
                        u.has_seen_cart_warning = true;
                        this.service.setUser(u);
                        cb();
                    } else {
                        this.state = 'add';
                    }
                });
            }
        } else if (this.state === 'library') {
            const cartable = this.cartable;
            if (this.isPackage(cartable)) {
                this.router.navigate(['/videos', cartable.videos[0].id, 'watch'], { queryParams: { package: this.id }});
            } else {
                this.router.navigate(['/videos', this.id, 'watch']);
            }
        }
    }

}
