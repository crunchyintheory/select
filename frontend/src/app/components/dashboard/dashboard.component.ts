import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { SettingService } from 'src/app/api/models/setting';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

    private _menu: { [key: string]: { name: string, icon: string, display: string, badge?: number } } = {
        settings: { name: 'settings', icon: 'cog', display: 'Settings' },
        subscriptions: { name: 'subscriptions', icon: 'money-bill', display: 'Subscriptions', badge: 0 },
        videos: { name: 'videos', icon: 'film', display: 'Videos', badge: 0 },
        packages: { name: 'packages', icon: 'layer-group', display: 'Packages', badge: 0 },
        upload: { name: 'upload', icon: 'upload', display: 'Upload' },
        sep1: { name: '', icon: '', display: '' },
        promos: { name: 'promos', icon: 'terminal', display: 'Promo Codes' },
        'gift-cards': { name: 'gift-cards', icon: 'credit-card', display: 'Gift Cards' },
        sep2: { name: '', icon: '', display: '' },
        users: { name: 'users', icon: 'users', display: 'Users', badge: 0 }
    };

    public get menu() {
        return Object.values(this._menu);
    }

    public activated = 'videos';

    constructor(public route: ActivatedRoute, public data: DataService, public settings: SettingService) {
        route.url.subscribe(() => {
            this.activated = route.snapshot.firstChild.routeConfig.path;
        });

        this.data.get(`/dashboard`).subscribe((x: { videos: number, packages: number, users: number }) => {
            this._menu.videos.badge = x.videos;
            this._menu.packages.badge = x.packages;
            this._menu.users.badge = x.users;
        });

        this.settings.settings.subscribe(x => {
            this._menu.videos.display = x.video_name_plural;
            this._menu.packages.display = x.package_name_plural;
        });
    }
}
