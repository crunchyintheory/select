import { Component, OnInit } from '@angular/core';
import { Paginator } from 'src/app/api/Paginator';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Promo, PromoService } from 'src/app/api/models/promo';

@Component({
  selector: 'app-promos-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManagePromosComponent implements OnInit {

  public pager: Paginator<Promo[]>;

  public promos = new Array<Promo>();

  public searchInput = '';

  public onSearch = new Subject<string>();

  public isEnd = false;

  public startFresh = false;

  public deleted = false;

  public readonly now = new Date();

  constructor(public service: PromoService) { }

  ngOnInit(): void {
    this.pager = this.service.list().paginate(20).start(x => this.addMore(x));
    this.onSearch.pipe(
      debounceTime(1000)
    ).subscribe(x => {
      this.promos = [];
      this.pager.complete();
      this.startFresh = true;
      this.pager = this.service.list(x).paginate(20).start(y => this.addMore(y));
    });
  }

  public addMore(promos: Promo[]) {
    if (this.startFresh) {
      this.startFresh = false;
      this.promos = [];
    }

    this.isEnd = !promos.length;

    this.promos = this.promos.concat(promos);
  }

  public status(str: string): string {
    return str[0].toUpperCase() + str.substr(1);
  }

  public search() {
    this.onSearch.next(this.searchInput);
  }

  public loadMore() {
    this.pager.next();
  }

  public timecmp(a: string | number | Date, b: string | number | Date) {
    const A = new Date(a);
    const B = new Date(b);

    return A > B;
  }

}
