import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { SettingService } from 'src/app/api/models/setting';
import { Promo, PromoService } from 'src/app/api/models/promo';

@Component({
    selector: 'app-promo-edit-component',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditPromoComponent implements OnInit {

    public create = null;

    public promo: Promo;
    public token: string;

    public form: FormGroup;

    public tags = [];
    public ready = false;

    public tagLoading = false;

    public showMore = false;

    public exhausted = false;
    public startFresh = false;
    public onSearch = new Subject<string>();

    // tslint:disable-next-line: max-line-length
    constructor(public service: PromoService, public route: ActivatedRoute, public fb: FormBuilder, protected router: Router, public settings: SettingService) {
        this.form = this.fb.group({
            code: ['', Validators.required],
            discount: [''],
            expires_date: [''],
            expires_time: [new Date()]
        });
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(paramMap => {
            const id = paramMap.get('id');
            this.create = !id;
            if (this.create) {
                this.promo = new Promo();
                this.ready = true;
            } else {
                this.service.get(id).subscribe(x => {
                    this.promo = new Promo(x);
                    const { code, expires, discount } = x;
                    // tslint:disable-next-line: max-line-length
                    this.form.setValue({ code, expires_date: new Date(expires), expires_time: new Date(expires), discount: discount / 100 });
                    this.ready = true;
                });
            }
        });
    }

    public onSubmit() {
        const expires = new Date(this.form.get('expires_date').value);
        const time = this.form.get('expires_time').value as Date;
        expires.setHours(time.getHours());
        expires.setMinutes(time.getMinutes());
        const data: Promo = {
            code: this.form.get('code').value as string,
            discount: this.form.get('discount').value as number,
            expires
        };
        if (this.create) {
            this.service.store(data).subscribe(() => {
                this.router.navigate(['/dashboard/promos']);
            });
        } else {
            this.service.update(this.promo.id, data).subscribe(() => {
                this.router.navigate(['/dashboard/promos']);
            });
        }
    }

    public delete() {
        this.service.destroy(this.promo.id).subscribe(() => {
            this.router.navigate(['/dashboard/promos']);
        });
    }

}
