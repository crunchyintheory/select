import { Component } from '@angular/core';
import { SettingService } from 'src/app/api/models/setting';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {

    public settings: { [key: string]: string } = {};

    public form: FormGroup;

    constructor(public service: SettingService, public builder: FormBuilder) {
        this.form = this.builder.group({
            site_name: [''],
            package_name: [''],
            package_name_plural: [''],
            package_name_plural_listing: [''],
            video_name: [''],
            video_name_plural: [''],
            video_name_plural_listing: [''],
            login_page_text: [''],
            home_page_text: [''],
            org_name: [''],
            org_logo: [''],
            footer_left_text: [''],
            footer_right_text: [''],
            subscribe_page_text: [''],
            manage_subscription_dialog_text: ['']
        });
    }

    public ngOnInit() {
        this.service.settings.subscribe(x => {
            console.log(x);
            this.form.patchValue(x);
        });
    }

    public onSubmit() {
        this.service.update(this.form.value).subscribe();
    }
}
