import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { SettingService } from 'src/app/api/models/setting';
import { GiftCard, GiftCardService } from 'src/app/api/models/giftCard';

@Component({
    selector: 'app-gift-card-edit-component',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditGiftCardComponent implements OnInit {

    public create = null;

    public giftCard: GiftCard;
    public token: string;

    public form: FormGroup;

    public tags = [];
    public ready = false;

    public tagLoading = false;

    public showMore = false;

    public exhausted = false;
    public startFresh = false;
    public onSearch = new Subject<string>();

    // tslint:disable-next-line: max-line-length
    constructor(public service: GiftCardService, public route: ActivatedRoute, public fb: FormBuilder, protected router: Router, public settings: SettingService) {
        this.form = this.fb.group({
            name: ['', Validators.required],
            value: ['', Validators.max(10000)],
            redemption: [''],
            count: [1]
        });
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(paramMap => {
            const id = paramMap.get('id');
            this.create = !id;
            if (this.create) {
                this.giftCard = new GiftCard();
                this.ready = true;
            } else {
                this.service.get(id).subscribe(x => {
                    this.giftCard = new GiftCard(x);
                    const { name, redemption, value } = x;
                    // tslint:disable-next-line: max-line-length
                    this.form.setValue({ name, redemption, value: (value / 100).toFixed(2), count: 1 });
                    this.ready = true;
                });
            }
        });
    }

    public onSubmit() {
        const data = {
            name: this.form.get('name').value as string,
            redemption: this.form.get('redemption').value as string,
            value: this.form.get('value').value as number,
            count: this.form.get('count').value as number
        };
        if (this.create) {
            this.service.store(data).subscribe(() => {
                this.router.navigate(['/dashboard/gift-cards']);
            });
        } else {
            this.service.update(this.giftCard.id, data).subscribe(() => {
                this.router.navigate(['/dashboard/gift-cards']);
            });
        }
    }

    public delete() {
        this.service.destroy(this.giftCard.id).subscribe(() => {
            this.router.navigate(['/dashboard/gift-cards']);
        });
    }

}
