import { Component, OnInit } from '@angular/core';
import { Paginator } from 'src/app/api/Paginator';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { GiftCard, GiftCardService } from 'src/app/api/models/giftCard';

@Component({
  selector: 'app-gift-cards-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageGiftCardsComponent implements OnInit {

  public pager: Paginator<GiftCard[]>;

  public giftCards = new Array<GiftCard>();

  public searchInput = '';

  public onSearch = new Subject<string>();

  public isEnd = false;

  public startFresh = false;

  public deleted = false;

  constructor(public service: GiftCardService) { }

  ngOnInit(): void {
    this.pager = this.service.list().paginate(20).start(x => this.addMore(x));
    this.onSearch.pipe(
      debounceTime(1000)
    ).subscribe(x => {
      this.giftCards = [];
      this.pager.complete();
      this.startFresh = true;
      this.pager = this.service.list(x).paginate(20).start(y => this.addMore(y));
    });
  }

  public addMore(giftCards: GiftCard[]) {
    if (this.startFresh) {
      this.startFresh = false;
      this.giftCards = [];
    }

    this.isEnd = !giftCards.length;

    this.giftCards = this.giftCards.concat(giftCards);
  }

  public status(str: string): string {
    return str[0].toUpperCase() + str.substr(1);
  }

  public search() {
    this.onSearch.next(this.searchInput);
  }

  public loadMore() {
    this.pager.next();
  }

  public del(giftCard: GiftCard, i: number) {
    this.giftCards.splice(i, 1);
    this.service.destroy(giftCard.id).subscribe(() => {
      this.startFresh = true;
      this.onSearch.next(this.searchInput);
    });
  }

}
