import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { UserService } from 'src/app/api/models/user';
import { Router } from '@angular/router';
import { LoginComponent } from './login.component';
import { Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { SettingService } from 'src/app/api/models/setting';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-register',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class RegisterComponent extends LoginComponent {

    public isLogin = false;

    constructor(protected data: DataService, protected user: UserService, protected router: Router, protected fb: FormBuilder, public settings: SettingService, protected sanitizer: DomSanitizer) {
        super(data, user, router, fb, settings, sanitizer);
        this.form = this.fb.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            confirmPassword: ['']
        }, { validator: this.checkPasswords });
    }

    public checkPasswords(group: AbstractControl) {
        /* eslint-disable @typescript-eslint/no-unsafe-assignment */
        const password = group.get('password').value;
        const confirmPass = group.get('confirmPassword').value;

        if (password && confirmPass && password !== confirmPass) {
            group.get('confirmPassword').setErrors({ ConfirmPassword: true });
            group.get('confirmPassword').markAsTouched();
        } else {
            return null;
        }
    }

    public submit() {
      /* eslint-disable @typescript-eslint/no-unsafe-assignment */
      this.data.post('register',
        {
          first_name: this.form.get('first_name').value,
          last_name: this.form.get('last_name').value,
          email: this.form.get('email').value,
          password: this.form.get('password').value,
          password_confirmation: this.form.get('confirmPassword').value
        }
      ).credentials().subscribe(() => {
        this.user.current(true).subscribe();
        this.router.navigate(['/home']);
      }, (resp: { error?: { errors: { [key: string]: string[] } } }) => {
        this.validated = true;
        if (resp.error?.errors) {
            const errors: Array<[string, Array<string>]> = Object.entries(resp.error.errors);
            for (const [field] of errors) {
                const control = this.form.get(field);
                control?.setErrors({invalid: true});
                control?.markAsTouched();
            }
        }
      });
    }

}
