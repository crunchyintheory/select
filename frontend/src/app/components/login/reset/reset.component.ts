import { Component } from "@angular/core";
import { UserService } from 'src/app/api/models/user';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './reset.component.html',
    styleUrls: ['./reset.component.scss']
})
export class ForgotPasswordComponent {
    public submitting = false;
    public done = false;
    public email = '';

    constructor(private user: UserService) {}
    
    public submit() {
        if(this.submitting) {
            return;
        }
        this.submitting = true;
        this.user.sendPasswordResetEmail(this.email).subscribe(() => {
            this.done = true;
        });
    }
}