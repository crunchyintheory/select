import { Component, SecurityContext } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { UserService } from 'src/app/api/models/user';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { first } from 'rxjs/operators';
import { SettingService } from 'src/app/api/models/setting';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import marked from 'marked';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    public isLogin = true;

    public validated = false;

    public form: FormGroup;

    public copy: SafeHtml;

    constructor(protected data: DataService, protected user: UserService, protected router: Router, protected fb: FormBuilder, public settings: SettingService, protected sanitizer: DomSanitizer) {
        this.form = this.fb.group({
            email: [''],
            password: ['']
        });
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        this.user.user_or_loggedout.pipe(first(x => !!x)).subscribe(() => this.router.navigate(['/home']));
        this.settings.settings.subscribe(x => {
            if(Object.values(x).length) {
                this.copy = this.sanitizer.sanitize(SecurityContext.HTML, marked(x.login_page_text));
            }
        });
    }

    public submit() {
        /* eslint-disable @typescript-eslint/no-unsafe-assignment */
        this.data.post('login',
            {
                email: this.form.get('email').value,
                password: this.form.get('password').value
            },
            false
        ).credentials().subscribe(() => {
            this.user.current(true).subscribe();
            this.router.navigate(['/home']);
        }, (resp: { error?: { errors: { [key: string]: string[] } } }) => {
            this.validated = true;
            if (resp.error?.errors) {
                const errors: Array<[string, Array<string>]> = Object.entries(resp.error.errors);
                for (const [field] of errors) {
                    const control = this.form.get(field);
                    control?.setErrors({invalid: true});
                    control?.markAsTouched();
                }
            }
        });
    }

}
