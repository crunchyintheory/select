import { Component, SecurityContext, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import marked from 'marked';
import { HttpClient } from '@angular/common/http';

type Page = { template: string, format: string };

@Component({
    selector: 'app-help',
    template: `<div class="container" [innerHTML]='activatedPage'></div>`
})
export class HelpComponent implements OnInit {

    public activatedPage: SafeHtml;

    public readonly pages: { [key: string]: Page } = {
        markdown: { template: 'markdown.md', format: 'markdown' }
    };

    public constructor(private route: ActivatedRoute, private http: HttpClient, private sanitizer: DomSanitizer) {}

    public ngOnInit() {
        this.route.params.subscribe(x => {
            console.log(x);
            const page = this.pages[x.page as string];
            if (page) {
                this.http.get(`/help/${page.template}`, { responseType: 'text' }).subscribe(x => {
                    switch(page.format) {
                        case 'markdown':
                            this.activatedPage = this.sanitizer.sanitize(SecurityContext.HTML, marked(x));
                    }
                });
            }
        });
    }

}