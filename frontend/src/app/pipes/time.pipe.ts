import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  private breakpoints: Array<[number, string, boolean]> = [
    [0, 'second', true],
    [2, 'seconds', false],
    [60, 'minute', true],
    [180, 'minutes', false],
    [3600, 'hour', true],
    [7200, 'hours', false],
    [86400, 'day', true],
    [172800, 'days', false],
    [604800, 'week', true],
    [1209600, 'weeks', false],
    [2629800, 'month', true],
    [5259600, 'months', false],
    [31557600, 'year', true],
    [63115200, 'years', false]
  ];

  transform(value: number, precision = 0): string { // Takes number in seconds and returns in nice format
    let newval = value;
    let label = 'second';

    for (const [point, name, div] of this.breakpoints) {
      if (Math.abs(value) >= point) {
        newval = div ? value / Math.max(point, 1) : newval;
        label = name;
      } else {
        break;
      }
    }

    return `${newval.toFixed(precision)} ${label}`;
  }

}
