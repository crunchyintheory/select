import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'clock'
})
export class ClockPipe implements PipeTransform {

  transform(value: number): string { // Takes number in seconds and returns in nice format

    const hours = Math.floor(value / 3600);
    const minutes = Math.floor(value / 60 - hours * 60);
    const seconds = Math.floor(value - Math.floor(value / 60) * 60);

    const displaySeconds = (seconds < 10 ? '0' : '') + seconds.toString();
    const displayMinutes = (minutes < 10 ? '0' : '') + minutes.toString() + ':';
    const displayHours = hours > 0 ? (hours < 10 ? '0' : '') + hours.toString() + ':' : '';

    return displayHours + displayMinutes + displaySeconds;
  }

}
