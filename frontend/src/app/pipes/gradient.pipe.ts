import { Pipe, PipeTransform } from '@angular/core';
import { Color } from '../color';

@Pipe({
  name: 'gradient'
})
export class GradientPipe implements PipeTransform {

  private angles = [
    20,
    147
  ];

  transform(value: string): string {

    const [h, s, v] = (new Color(value)).hsl();

    const rand = Math.abs(Math.floor(Math.sin(h * s * v) / (2 * Math.PI) * this.angles.length));

    // background-image: linear-gradient(147deg, #FFE53B 0%, #FF2525 74%);

    return `linear-gradient(${this.angles[rand]}deg, hsl(${h}, ${s}%, 45%) 26%, hsl(${h + 50}, ${s - 10}%, 70%))`;
  }

}
