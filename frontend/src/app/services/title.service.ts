import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SettingService } from '../api/models/setting';
import { ReplaySubject } from 'rxjs';
import { takeLast } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

    private sets = new ReplaySubject<{ [key: string]: string }>();

    constructor(private titleService: Title, private settings: SettingService) {
        this.settings.settings.subscribe(x => {
            if(x.site_name) {
                this.sets.next(x);
                this.sets.complete();
            }
        });
    }

    public setTitle(name?: string) {
        this.sets.pipe(takeLast(1)).subscribe(x => {
            if(!name) {
                return this.titleService.setTitle(x.site_name);
            }
            if(name.slice(0, 1) === '~') {
                return this.titleService.setTitle(`${x.site_name} - ${x[name.slice(1)]}`);
            }
            return this.titleService.setTitle(`${x.site_name} - ${name}`);
        });
    }
}
