/* eslint-disable @typescript-eslint/prefer-regexp-exec */
import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { RequestBuilderOptions } from '../api/RequestBuilderOptions';
import resources, { StaticResource } from '../data/static';

@Injectable({
    providedIn: 'root'
})
export class StaticService {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public handler<T>(method: string, uri: string, args: any[]): Observable<T> {
        switch (method) {
            case "get": return this.get(uri, args[0]);
            case "post": return this.post(uri, args[0], args[1]);
            case "put": return this.put(uri, args[0], args[1]);
            case "delete": return this.delete(uri, args[0]);
        }
    }

    public get<T>(uri: string, data: RequestBuilderOptions): Observable<T> {
        return from(resources).pipe(
            first(x => x.method == "get" && uri.match(x.pattern) !== null),
            map((x: StaticResource<[string, RequestBuilderOptions], T>) => x.response([uri, data]))
        );
    }

    public post<T, Resp = unknown>(uri: string, data: RequestBuilderOptions, body: T): Observable<Resp> {
        return from(resources).pipe(
            first(x => x.method == "post" && uri.match(x.pattern) !== null),
            map((x: StaticResource<[RequestBuilderOptions, T], Resp>) => x.response([data, body]))
        );
    }

    public put<T, Response = unknown>(uri: string, data: RequestBuilderOptions, body: T): Observable<Response> {
        return from(resources).pipe(
            first(x => x.method == "post" && uri.match(x.pattern) !== null),
            map((x: StaticResource<[RequestBuilderOptions, T], Response>) => x.response([data, body]))
        );
    }

    public delete<Response = unknown>(uri: string, data: RequestBuilderOptions): Observable<Response> {
        return from(resources).pipe(
            first(x => x.method == "post" && uri.match(x.pattern) !== null),
            map((x: StaticResource<RequestBuilderOptions, Response>) => x.response(data))
        );
    }
}
