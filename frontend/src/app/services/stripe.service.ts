import { Injectable } from '@angular/core';
import { loadStripe, Stripe } from '@stripe/stripe-js';
import { DataService } from './data.service';
import { map, exhaust } from 'rxjs/operators';
import { RequestBuilder } from '../api/RequestBuilder';
import { Promo } from '../api/models/promo';
import { Router } from '@angular/router';
import { User, UserService } from '../api/models/user';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  // tslint:disable-next-line: variable-name
  private static _stripe: Stripe;
  protected get stripe() {
    return StripeService._stripe;
  }
  protected set stripe(val) {
    StripeService._stripe = val;
  }

  constructor(protected data: DataService, protected router: Router, protected user: UserService) {
    if (!this.stripe) {
      data.get<string>('/stripeKey').pipe(
        map(pKey => loadStripe(pKey)),
        exhaust()
      ).subscribe(stripe => {
        this.stripe = stripe;
      });
    }
  }

  public checkout(): void {
    this.data.get<string | User>('/checkout').subscribe(result => {
      if (typeof result === 'string') {
        this.stripe.redirectToCheckout({ sessionId: result }).then(({ error }) => {
          console.log(error);
        }).catch(error => {
          console.log(error);
        });
      } else {
        this.user.current(true).subscribe();
        this.router.navigate(['/videos', 'library']);
      }
    });
  }

  public subscribe(subscription: number): void {
    this.data.post<unknown, string>(`/checkout/subscriptions/${subscription}/session`, {}, false).subscribe(result => {
      this.stripe.redirectToCheckout({ sessionId: result }).then(({ error }) => {
        console.log(error);
      }).catch(error => {
        console.log(error);
      });
    }, () => {
      Swal.fire({
        title: 'Error',
        html: `You are already subscribed!<br/>Go to "Manage My Subscription" to unsubscribe.`,
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        icon: 'error'
      });
    });
  }

  public applyPromo(code: string): RequestBuilder<Promo> {
    return this.data.post<{ code: string }, Promo>('/cart/promo', { code });
  }

  public removePromo(): RequestBuilder<never> {
    return this.data.delete<never>('/cart/promo');
  }
}
