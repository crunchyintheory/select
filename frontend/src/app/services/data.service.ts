import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, take, exhaust } from 'rxjs/operators';
import { RequestBuilder } from '../api/RequestBuilder';
import { GetRequestBuilder } from '../api/GetRequestBuilder';
import { staticify, Staticify } from 'staticify.js';
import { APIService } from './api.service';
import { StaticService } from './static.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(public http: HttpClient) {
    const api = new APIService(http);
    const stat = new StaticService();
    const pattern = /^\/(.+)/i;
    Staticify.local = environment.static;
    /* eslint-disable @typescript-eslint/no-unsafe-call */
    Staticify.get(pattern, api.get.bind(api), stat.get);
    Staticify.post(pattern, api.post.bind(api), stat.post);
    Staticify.put(pattern, api.put.bind(api), stat.put);
    Staticify.delete(pattern, api.delete.bind(api), stat.delete);
    /* eslint-enable @typescript-eslint/no-unsafe-call */
  }

  public get<T>(slug: string, errorToast = true): GetRequestBuilder<T> {
    if (slug.substr(0, 1) !== '/') {
      slug = '/' + slug;
    }

    return new GetRequestBuilder((data) => {
      return data.queryString().pipe(
        take(1),
        map(queryString => staticify<Observable<T>>("get", `/api${slug}${queryString}`, [data, errorToast])),
        exhaust()
      );
    });
  }

  public post<T, Resp = unknown>(slug: string, body: T, errorToast = true): RequestBuilder<Resp> {
    if (slug.substr(0, 1) !== '/') {
      slug = '/' + slug;
    }
    return new RequestBuilder((data) =>
      staticify<Observable<Resp>>("post", `/api${slug}`, [data, body, errorToast])
    );
  }

  public put<T, Response = unknown>(slug: string, body: T, errorToast = true): RequestBuilder<Response> {
    if (slug.substr(0, 1) !== '/') {
      slug = '/' + slug;
    }
    return new RequestBuilder((data) => 
      staticify<Observable<Response>>("put", `/api${slug}`, [data, body, errorToast])
    );
  }

  public delete<Response = unknown>(slug: string, errorToast = true): RequestBuilder<Response> {
    if (slug.substr(0, 1) !== '/') {
      slug = '/' + slug;
    }
    return new RequestBuilder((data) => 
      staticify<Observable<Response>>("delete", `/api${slug}`, [data, errorToast])
    );
  }

  public csrf(): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return (document.querySelector('meta[name="csrf-token"]') as HTMLMetaElement).content;
  }
}
