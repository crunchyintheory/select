import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, filter } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { RequestBuilderOptions } from '../api/RequestBuilderOptions';

type Error = { headers: unknown, message: string, name: string, ok: boolean, status: number, statusText: string, url: string };

export class APIService {

    constructor(public http: HttpClient) {
    }

    /* eslint-disable @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unused-vars */
    private errorHandler<T>(errorSwal: boolean): (err: Error, caught: Observable<T>) => Observable<never> {
        return (err: Error, caught: Observable<T>) => {
            if (errorSwal) {
                console.error(err);
                Swal.fire({
                    title: 'Error',
                    text: `${err.status}: ${err.statusText}`,
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 6000,
                    icon: 'error'
                });
                return new Observable();
            }
            throw err;
        };
    }

    public get<T>(uri: string, data: RequestBuilderOptions, errorToast: boolean): Observable<T> {
        return this.http.get<T>(uri, data).pipe(
            filter(x => x !== null),
            catchError(this.errorHandler(errorToast))
        );
    }

    public post<T, Resp = unknown>(uri: string, data: RequestBuilderOptions, body: T, errorToast: boolean): Observable<Resp> {
        return this.http.post<Resp>(uri, body, data).pipe(
            catchError(this.errorHandler(errorToast))
        );
    }

    public put<T, Response = unknown>(uri: string, data: RequestBuilderOptions, body: T, errorToast: boolean): Observable<Response> {
        return this.http.put<Response>(uri, body, data).pipe(
            catchError(this.errorHandler(errorToast))
        );
    }

    public delete<Response = unknown>(uri: string, data: RequestBuilderOptions, errorToast: boolean): Observable<Response> {
        return this.http.delete<Response>(uri, data).pipe(
            catchError(this.errorHandler(errorToast))
        );
    }
}
