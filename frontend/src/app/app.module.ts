import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { NgProgressModule } from 'ngx-progressbar';
import { NgProgressHttpModule } from 'ngx-progressbar/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CloudflareStreamModule } from '@cloudflare/stream-angular';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './components/login/register.component';
import { UploadComponent } from './components/videos/upload/upload.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ListVideosComponent } from './components/videos/list/list.component';
import { SingleVideoComponent } from './components/videos/single/single.component';
import { HomeComponent } from './components/home/home.component';
import { EmojiComponent } from './components/emoji/emoji.component';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { TimePipe } from './pipes/time.pipe';
import { GradientPipe } from './pipes/gradient.pipe';
import { ClockPipe } from './pipes/clock.pipe';
import { ManagePackagesComponent } from './components/packages/manage/manage.component';

import { PurchaseButtonComponent } from './components/purchase-button/purchase-button.component';
import { VideoBubbleComponent } from './components/videos/bubble/bubble.component';
import { TagComponent } from './components/tag/tag.component';
import { PackageBubbleComponent } from './components/packages/bubble/bubble.component';
import { ListPackagesComponent } from './components/packages/list/list.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ManageVideosComponent } from './components/videos/manage/manage.component';
import { PricePipe } from './pipes/price.pipe';
import { EditVideoComponent } from './components/videos/edit/edit.component';
import { EditPackageComponent } from './components/packages/edit/edit.component';
import { LibraryComponent } from './components/videos/library/library.component';
import { WatchVideoComponent } from './components/videos/watch/watch.component';
import { PlayerComponent } from './components/videos/player/player.component';
import { PaymentCompleteComponent } from './components/payment/complete/complete.component';
import { SettingsComponent } from './components/dashboard/settings/settings.component';
import { SettingService } from './api/models/setting';
import { RedeemComponent } from './components/redeem/redeem.component';
import { RedeemInputComponent } from './components/redeem/input.component';
import { EditGiftCardComponent } from './components/dashboard/gift-cards/edit/edit.component';
import { EditPromoComponent } from './components/dashboard/promos/edit/edit.component';
import { ManageGiftCardsComponent } from './components/dashboard/gift-cards/manage/manage.component';
import { ManagePromosComponent } from './components/dashboard/promos/manage/manage.component';
import { FooterComponent } from './components/footer/footer.component';
import { HelpComponent } from './components/help/help.component';
import { ForgotPasswordComponent } from './components/login/reset/reset.component';
import { ManageUsersComponent } from './components/users/manage/manage.component';
import { EditUserComponent } from './components/users/edit/edit.component';
import { RecordsTableComponent } from './components/records/table/table.component';
import { SubscribeComponent } from './components/subscription/subscribe/subscribe.component';
import { ManageSubscriptionsComponent } from './components/subscription/manage/manage.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    UploadComponent,
    CheckoutComponent,
    ListVideosComponent,
    SingleVideoComponent,
    HomeComponent,
    EmojiComponent,
    PurchaseButtonComponent,
    TimePipe,
    GradientPipe,
    ManagePackagesComponent,
    VideoBubbleComponent,
    ClockPipe,
    TagComponent,
    PackageBubbleComponent,
    ListPackagesComponent,
    DashboardComponent,
    ManageVideosComponent,
    PricePipe,
    EditVideoComponent,
    EditPackageComponent,
    LibraryComponent,
    WatchVideoComponent,
    PlayerComponent,
    PaymentCompleteComponent,
    SettingsComponent,
    RedeemComponent,
    RedeemInputComponent,
    EditGiftCardComponent,
    EditPromoComponent,
    ManageGiftCardsComponent,
    ManagePromosComponent,
    FooterComponent,
    HelpComponent,
    ForgotPasswordComponent,
    ManageUsersComponent,
    EditUserComponent,
    RecordsTableComponent,
    SubscribeComponent,
    ManageSubscriptionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    NgProgressModule,
    NgProgressHttpModule,
    NgSelectModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    ReactiveFormsModule,
    CloudflareStreamModule,
    InfiniteScrollModule,
    SweetAlert2Module.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  providers: [SettingService],
  bootstrap: [AppComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule {
  constructor(private library: FaIconLibrary) {
    this.library.addIconPacks(fas);
  }
}
