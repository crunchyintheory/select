export class Color {

    private rgb: string;

    private r: number;
    private g: number;
    private b: number;

    public constructor(rgb: string) {
        this.rgb = rgb.replace('#', '');

        this.r = Number.parseInt(this.rgb.substr(0, 2), 16);
        this.g = Number.parseInt(this.rgb.substr(2, 2), 16);
        this.b = Number.parseInt(this.rgb.substr(4, 2), 16);
    }

    public hue() {
        const r = this.r / 255;
        const g = this.g / 255;
        const b = this.b / 255;

        const max = Math.max(r, g, b);
        const min = Math.min(r, g, b);

        if (max === r) {
            return (g - b) / (max - min);
        } else if (max === g) {
            return 2 + (b - r) / (max - min);
        } else if (max === b) {
            return 4 + (r - g) / (max - min);
        }
    }

    public hsl() {
        // https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
        const r = this.r / 255;
        const g = this.g / 255;
        const b = this.b / 255;

        const max = Math.max(r, g, b);
        const min = Math.min(r, g, b);

        // tslint:disable-next-line: one-variable-per-declaration prefer-const
        let h: number, s: number, v = (max + min) / 2;

        if (max === min) {
            h = 0;
            s = 0;
        } else {
            const d = max - min;
            s = v > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch (max) {
                case r: h = (g - b) / d + (g < b ? 6 : 0); break;
                case g: h = (b - r) / d + 2; break;
                case b: h = (r - g) / d + 4; break;
            }
            h /= 6;
        }

        return [h * 360, s * 100, v * 100];
    }

    public setValue(value: number) {
        const hsl = this.hsl();

        hsl[2] = value;

        return hsl;
    }
}
