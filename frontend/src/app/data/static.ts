export type StaticResource<T, K> = { method: string, pattern: RegExp, response: (args: T) => K }

const videos = [{
    id: "76fbd956-0907-11ec-9a03-0242ac130003",
    price: 500,
    duration: 500,
    thumbnail: { url: "https://via.placeholder.com/426x240.png?text=Thumbnail+Here" },
    name: "Welcome to Select!",
    cfstate: "ready",
    duration_tag: { id: 1, name: "", time_threshold: 300 },
    tags: [{id: 2, name: "Tag 2"}],
    description: "This short video will lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet metus a lacus congue commodo sed pretium ante. Fusce eu nisl rutrum, auctor ex vel, interdum leo."
},
{
    id: "819cffa2-0907-11ec-9a03-0242ac130003",
    price: 500,
    duration: 500,
    thumbnail: { url: "https://via.placeholder.com/426x240.png?text=Thumbnail+Here+TWO" },
    name: "Welcome to Select PART TWO!",
    cfstate: "ready",
    duration_tag: { id: 1, name: "", time_threshold: 300 },
    tags: [{id: 2, name: "Tag 2"}],
    description: "This short video will lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet metus a lacus congue commodo sed pretium ante. Fusce eu nisl rutrum, auctor ex vel, interdum leo."
}];

export const data: StaticResource<unknown, unknown>[] = [
    {
        method: "get",
        pattern: /^\/api\/user/,
        response: () => {
            return {
                id: 1,
                first_name: "Demo",
                last_name: "User",
                email: "demo@example.com",
                cart: [],
                videos: [],
                abilities: ["*"],
                roles: [{ abilities: ["*"] }],
                balance: 1000,
                email_verified_at: new Date(),
                has_seen_cart_warning: false,
                subscribed: false,
                stripe_id: null,
            };
        }
    },
    {
        method: "get",
        pattern: /^\/api\/settings/,
        response: () => {
            return [
                {"name":"site_name","value":"Select Demo"},
                {"name":"package_name","value":"Package"},
                {"name":"package_name_plural","value":"Packages"},
                {"name":"package_name_plural_listing","value":"Packages"},
                {"name":"video_name","value":"Video"},
                {"name":"video_name_plural","value":"Videos"},
                {"name":"video_name_plural_listing","value":"All Videos"},
                {"name":"login_page_text","value":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut convallis lacus, id placerat justo. Fusce sed finibus ipsum. Sed bibendum blandit metus in congue. Maecenas risus mi, bibendum ac massa sit amet, malesuada malesuada ex. Morbi convallis lectus sit amet enim facilisis, vitae sagittis neque condimentum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi vel iaculis metus. Vivamus convallis consequat mauris et pharetra. Phasellus vel nibh id sem consectetur consectetur sit amet id felis. Quisque ipsum ligula, commodo quis euismod ut, rutrum at massa. Morbi bibendum tempus nunc ac tincidunt. Donec in felis condimentum, finibus dolor sed, consequat purus. Etiam luctus ex mauris, sed dignissim nulla porttitor ut. Nunc dolor felis, molestie consectetur turpis et, fringilla faucibus sapien. Suspendisse sit amet massa tincidunt, dapibus turpis quis, dapibus ligula. Nulla scelerisque dignissim volutpat. Nam eget volutpat nisi. Nunc hendrerit lacus nec mauris molestie, ac varius leo facilisis."},
                {"name":"org_name","value":"Select Team"},
                {"name":"org_logo","value":"/favicon.ico"},
                {"name":"home_page_text","value":"Hello and welcome to the Select demo! Please note that currently, the administration page is disabled as is video streaming and making purchases. This site is designed to showcase Select's versatile UI and will be adding more features soon."},
                {"name":"footer_left_text","value":"*Left Footer Text*"},
                {"name":"footer_right_text","value":"*Right Footer Text*"},
                {"name":"subscribe_page_text","value":"*Subscribe Page Text*"},
                {"name":"manage_subscription_dialog_text","value":"You will be taken to Stripe, where you can cancel your subscription or update your payment method."}
            ];
        }
    },
    {
        method: "get",
        pattern: /^\/api\/packages/i,
        response: ([uri, data]) => {
            const url = new URL(uri, window.location.toString());
            if(url.searchParams.get("skip") != "0") return [];
            return [
            {
                bgcolor: "#21d4fd",
                description: "A bundle of videos to help you get started on your journey.",
                id: "8e581a7e-0907-11ec-9a03-0242ac130003",
                name: "The Welcome Bundle",
                price: 900,
                tags: [{ id: 2, name: "Tag 2" }],
                videos: videos
            }];
        }
    },
    {
        method: "get",
        pattern: /^\/api\/videos\/featured/i,
        response: () => videos[0]
    },
    {
        method: "get",
        pattern: /^\/api\/videos($|\?.+)/i,
        response: ([uri, data]) => {
            const url = new URL(uri, window.location.toString());
            if(url.searchParams.get("skip") != "0") return [];
            return videos;
        }
    },
    {
        method: "get",
        pattern: /^\/api\/videos\/.+/i,
        response: ([uri, data]) => {
            const url = new URL(uri, window.location.toString());
            const id = url.pathname.split('/');
            return videos.find(x => x.id == id[id.length - 1]);
        }
    },
    {
        method: "get",
        pattern: /libnew/i,
        response: () => []
    },
    {
        method: "get",
        pattern: /stripeKey/,
        response: () => ""
    },
    {
        method: "get",
        pattern: /subscriptions/,
        response: () => [
            {name: "Subscription Plan 1", price: 900, credit: 1000, expanded: false},
            {name: "Subscription Plan 2", price: 2000, credit: 3000, expanded: false}
        ]
    }
];

export default data;