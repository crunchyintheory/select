#!/bin/sh

./node_modules/@angular/cli/bin/ng build --outputPath=../api/public/app --watch --resourcesOutputPath=./ng --index=false \
|| (npm rebuild node-sass && ./node_modules/@angular/cli/bin/ng build --outputPath=../api/public/app --watch --resourcesOutputPath=./ng --index=false)