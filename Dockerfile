FROM php:8-fpm-alpine
WORKDIR /app
RUN apk add --update nginx openssl php-common zip libzip-dev
RUN docker-php-ext-install pdo_mysql zip
RUN rm -f /var/cache/apk/*
ADD --chown=nginx:nginx build_api /app
ADD --chown=nginx:nginx build_frontend /app/public/app
ADD --chown=nginx:nginx build_index/index.blade.php /app/resources/views/index.blade.php
ADD entrypoint.sh /
ADD nginx/nginx.conf /etc/nginx/conf.d/default.conf
ADD nginx/php-fpm.conf /etc/php7/php-fpm.d/www.conf
#RUN php artisan storage:link
RUN mkdir -p /var/run/nginx /var/run/php
CMD /entrypoint.sh