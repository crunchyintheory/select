#!/bin/sh
sleep 5
php artisan config:cache
php artisan storage:link --relative >/dev/null
php artisan migrate --seed

if [ "${DEV}" == "1" ]
then
    php ./artisan serve --host=0.0.0.0
else
    php-fpm
    nginx -g 'daemon off;'
fi
